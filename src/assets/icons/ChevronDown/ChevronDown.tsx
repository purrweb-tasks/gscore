import React from "react";

import type { IconsProps } from "../IconsProps";

const ChevronDown: React.FC<IconsProps> = ({
  strokeColor = "white",
  strokeWidth = 2,
  width = 24,
  height = 24,
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M5 8.5L12 15.5L19 8.5"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ChevronDown;

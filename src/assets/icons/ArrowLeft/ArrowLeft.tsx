import React from "react";

import { IconsProps } from "../IconsProps";

const ArrowLeft: React.FC<IconsProps> = ({
  strokeColor = "white",
  strokeWidth = 2,
  width = 20,
  height = 20,
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.46088 1.53918L1.00001 10.0001L9.46088 18.4609"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1.00009 10L19 10"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ArrowLeft;

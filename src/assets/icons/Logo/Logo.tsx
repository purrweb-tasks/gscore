import React from "react";

import type { IconsProps } from "../IconsProps";

const Logo: React.FC<IconsProps> = () => {
  return (
    <svg
      width="43"
      height="42"
      viewBox="0 0 43 42"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M21.689 21.8398L42.689 21.4199V20.5798L21.689 20.1599V21.8398Z"
        fill="#FC5842"
      />
      <path
        d="M22.1091 10.4999H21.269L20.8491 0H22.5291L22.1091 10.4999Z"
        fill="white"
      />
      <path
        d="M21.269 31.5H22.1091L22.5291 41.9999H20.8491L21.269 31.5Z"
        fill="white"
      />
      <path
        d="M11.1889 20.5798V21.4199L0.688965 21.8398V20.1599L11.1889 20.5798Z"
        fill="white"
      />
      <path
        d="M29.4108 13.8723L28.8169 13.2784L35.9444 5.55676L37.1323 6.74479L29.4108 13.8723Z"
        fill="white"
      />
      <path
        d="M13.9673 28.1276L14.5612 28.7215L7.43363 36.4431L6.24561 35.2551L13.9673 28.1276Z"
        fill="white"
      />
      <path
        d="M28.8169 28.7215L29.4108 28.1276L37.1323 35.2551L35.9444 36.4431L28.8169 28.7215Z"
        fill="white"
      />
      <path
        d="M14.5612 13.2783L13.9673 13.8723L6.24561 6.74471L7.43363 5.55688L14.5612 13.2783Z"
        fill="white"
      />
      <path
        d="M18.0587 11.1385L17.2827 11.4599L12.8765 1.92003L14.4285 1.2771L18.0587 11.1385Z"
        fill="white"
      />
      <path
        d="M25.3193 30.8615L26.0954 30.54L30.5016 40.0799L28.9493 40.7229L25.3193 30.8615Z"
        fill="white"
      />
      <path
        d="M11.8277 24.63L12.1491 25.406L2.60924 29.8123L1.96631 28.2602L11.8277 24.63Z"
        fill="white"
      />
      <path
        d="M26.0954 11.4599L25.3193 11.1385L28.9493 1.2771L30.5016 1.92003L26.0954 11.4599Z"
        fill="white"
      />
      <path
        d="M17.2827 30.54L18.0587 30.8615L14.4285 40.7229L12.8765 40.0799L17.2827 30.54Z"
        fill="white"
      />
      <path
        d="M31.229 25.406L31.5503 24.63L41.4118 28.2602L40.7689 29.8123L31.229 25.406Z"
        fill="white"
      />
      <path
        d="M12.1491 16.5939L11.8277 17.3699L1.96631 13.7397L2.60924 12.1876L12.1491 16.5939Z"
        fill="white"
      />
      <path
        d="M20.0522 10.6198L19.2284 10.7836L16.7681 0.567349L18.4159 0.239624L20.0522 10.6198Z"
        fill="white"
      />
      <path
        d="M23.3252 31.3799L24.149 31.2162L26.6095 41.4324L24.9617 41.7601L23.3252 31.3799Z"
        fill="white"
      />
      <path
        d="M11.3089 22.6364L11.4727 23.4604L1.25644 25.9207L0.928711 24.2729L11.3089 22.6364Z"
        fill="white"
      />
      <path
        d="M27.8713 12.503L27.1729 12.0364L32.657 3.07263L34.0538 4.00595L27.8713 12.503Z"
        fill="white"
      />
      <path
        d="M15.506 29.4969L16.2044 29.9638L10.7201 38.9275L9.32324 37.994L15.506 29.4969Z"
        fill="white"
      />
      <path
        d="M30.186 27.1826L30.6527 26.4841L39.6164 31.9685L38.6831 33.3653L30.186 27.1826Z"
        fill="white"
      />
      <path
        d="M13.1916 14.8173L12.725 15.5157L3.76123 10.0314L4.69476 8.63452L13.1916 14.8173Z"
        fill="white"
      />
      <path
        d="M16.2044 12.0364L15.506 12.503L9.32324 4.00595L10.7201 3.07263L16.2044 12.0364Z"
        fill="white"
      />
      <path
        d="M27.1729 29.9638L27.8713 29.4969L34.0538 37.994L32.657 38.9275L27.1729 29.9638Z"
        fill="white"
      />
      <path
        d="M30.6527 15.5157L30.186 14.8173L38.6831 8.63452L39.6164 10.0316L30.6527 15.5157Z"
        fill="white"
      />
      <path
        d="M12.725 26.4841L13.1916 27.1826L4.69455 33.3653L3.76123 31.9685L12.725 26.4841Z"
        fill="white"
      />
      <path
        d="M24.149 10.7836L23.3252 10.6198L24.9617 0.239624L26.6095 0.567349L24.149 10.7836Z"
        fill="white"
      />
      <path
        d="M19.2284 31.2162L20.0522 31.3799L18.4159 41.7601L16.7681 41.4324L19.2284 31.2162Z"
        fill="white"
      />
      <path
        d="M31.9053 23.4604L32.0692 22.6364L42.4494 24.2729L42.1217 25.9207L31.9053 23.4604Z"
        fill="white"
      />
      <path
        d="M11.4727 18.5396L11.3089 19.3636L0.928711 17.727L1.25644 16.0792L11.4727 18.5396Z"
        fill="white"
      />
    </svg>
  );
};

export default Logo;

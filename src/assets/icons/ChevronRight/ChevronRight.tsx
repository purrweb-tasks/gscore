import React from "react";

import type { IconsProps } from "../IconsProps";

const ChevronRight: React.FC<IconsProps> = ({
  strokeColor = "white",
  strokeWidth = 2,
  width = 12,
  height = 20,
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 12 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1.5 19L10.5 10L1.5 1"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ChevronRight;

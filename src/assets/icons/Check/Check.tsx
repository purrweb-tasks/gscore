import React from "react";

import type { IconsProps } from "../IconsProps";

const Check: React.FC<IconsProps> = ({
  strokeColor = "white",
  strokeWidth = 2,
  width = 24,
  height = 24,
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.15979 13.4058L7.78275 19.0287L21.8402 4.97131"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Check;

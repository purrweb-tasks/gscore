import React from "react";

import { IconsProps } from "../IconsProps";

const ChevronLeft: React.FC<IconsProps> = ({
  strokeColor = "white",
  strokeWidth = 2,
  width = 24,
  height = 24,
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16.5 3L7.5 12L16.5 21"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ChevronLeft;

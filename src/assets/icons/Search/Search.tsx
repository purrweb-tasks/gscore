import React from "react";

import type { IconsProps } from "../IconsProps";

const Search: React.FC<IconsProps> = ({
  strokeColor = "white",
  strokeWidth = 2,
  width = 24,
  height = 24,
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.8889 19.4173C15.7981 19.4173 19.7778 15.4376 19.7778 10.5284C19.7778 5.61922 15.7981 1.63953 10.8889 1.63953C5.97969 1.63953 2 5.61922 2 10.5284C2 15.4376 5.97969 19.4173 10.8889 19.4173Z"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M21.9998 21.6395L17.1665 16.8062"
        stroke={strokeColor}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Search;

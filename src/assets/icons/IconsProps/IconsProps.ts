export interface IconsProps {
  width?: number;
  height?: number;
  strokeWidth?: number;
  strokeColor?: string;
}

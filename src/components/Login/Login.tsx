import React from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import {
  Controller,
  ErrorOption,
  SubmitHandler,
  useForm,
} from "react-hook-form";
import styled from "styled-components";

import { Button, Input } from "components";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";
import { validators } from "utils";

interface FormInputs {
  email: string;
  password: string;
}

const Login: React.FC<LoginProps> = ({ setNextStage }) => {
  const isLoading = useAppSelector(selectors.auth.getIsLoading);
  const dispatch = useAppDispatch();
  const {
    handleSubmit,
    setError,
    control,
    formState: { isValid },
  } = useForm<FormInputs>({
    mode: "all",
    resolver: yupResolver(validators.loginValidator),
  });
  const onSubmit: SubmitHandler<FormInputs> = async (data) => {
    const { payload, meta } = await dispatch(actions.auth.signInThunk(data));
    if (meta.requestStatus === "rejected") {
      return setError("password", payload as ErrorOption);
    }
    if (!isLoading) {
      setNextStage(3);
    }
  };
  return (
    <Root>
      <Header>Log in</Header>
      <LoginForm onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name="email"
          render={({ field: { onChange, onBlur }, fieldState }) => (
            <Input
              type="email"
              placeholder="Email"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <Controller
          control={control}
          name="password"
          render={({ field: { onChange, onBlur }, fieldState }) => (
            <Input
              type="password"
              placeholder="Password"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <LoginButton>
          <Button
            disabled={!isValid || isLoading}
            variant="primary"
            value="Log in"
            isLoading={isLoading}
          />
        </LoginButton>
      </LoginForm>
    </Root>
  );
};

export default Login;

interface LoginProps {
  setNextStage: (nextStage: number) => void;
}
const Root = styled.div`
  height: 800px;
  width: 90%;
`;
const Header = styled.div`
  ${TYPOGRAPHY.headerL}
  padding-top: 60px;
  padding-bottom: 30px;
  color: ${({ theme }) => theme.colors.color1};
`;
const LoginForm = styled.form`
  width: 100%;
`;
const LoginButton = styled.div`
  width: 200px;
`;

import React from "react";

import styled from "styled-components";

import { Check, devices } from "assets";
const Checkbox: React.FC<CheckboxProps> = (props) => {
  return (
    <Root>
      <Icon>
        <Check width={16} height={16} />
      </Icon>
      <CheckboxInput {...props} type="checkbox" name="Checkbox" />
      <Label htmlFor="Checkbox">{props.value}</Label>
    </Root>
  );
};

export default Checkbox;

interface CheckboxProps extends React.InputHTMLAttributes<HTMLInputElement> {}

const Root = styled.div`
  position: relative;
`;
const Icon = styled.div`
  position: absolute;
  left: 25%;
  top: 20%;
  @media ${devices.mobileL} {
    left: 16%;
    top: 12%;
  }
`;
const CheckboxInput = styled.input`
  position: absolute;
  z-index: 0;
  opacity: 0;
  width: 28px;
  height: 28px;
  @media ${devices.mobileL} {
    width: 20px;
    height: 20px;
  }
  + label::before {
    content: "";
    display: inline-block;
    width: 28px;
    height: 28px;
    flex-shrink: 0;
    flex-grow: 0;
    border: 1px solid ${({ theme }) => theme.colors.color4};
    border-radius: 7px;
    background-color: ${({ theme }) => theme.colors.color1};
    background-repeat: no-repeat;
    background-position: center center;
    background-size: 50% 50%;
    @media ${devices.mobileL} {
      width: 20px;
      height: 20px;
    }
  }
  :hover + label::before {
    background-color: ${({ theme }) => theme.colors.color4};
  }
  + label {
    display: inline-flex;
    align-items: center;
    user-select: none;
  }
  :checked + label::before {
    background-color: ${({ theme }) => theme.colors.primary};
    border-color: ${({ theme }) => theme.colors.primary};
    :hover {
      background-color: ${({ theme }) => theme.colors.red4};
      z-index: 10;
    }
  }
  :checked + label::before:hover {
    background-color: ${({ theme }) => theme.colors.red4};
    z-index: 10;
  }
`;
const Label = styled.label`
  color: ${({ theme }) => theme.colors.color1};
`;

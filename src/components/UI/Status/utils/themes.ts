import { css } from "styled-components";

import { COLORS } from "styles";
import { Status } from "types";

export const THEMES = (status: "ACTIVE" | "INACTIVE" | "HOLD") => {
  switch (status) {
    case "ACTIVE": {
      return css`
        color: ${COLORS.green};
      `;
    }
    case "INACTIVE": {
      return css`
        color: ${COLORS.red3};
      `;
    }
    case "HOLD": {
      return css`
        color: ${COLORS.orange};
      `;
    }
  }
};

import styled from "styled-components";

import { devices } from "assets";
import { TYPOGRAPHY } from "styles";

import { THEMES } from "./utils/themes";

enum CardStatus {
  ACTIVE = "Active",
  INACTIVE = "Inactive",
  HOLD = "Hold",
}

const Status: React.FC<StatusProps> = ({ status }) => {
  return <Root status={status}> {CardStatus[status]} </Root>;
};

export default Status;

interface StatusProps {
  status: "ACTIVE" | "INACTIVE" | "HOLD";
}
const Root = styled.div<StatusProps>`
  ${TYPOGRAPHY.textBold};
  font-size: 22px;
  ${({ status }) => THEMES(status)}
  @media ${devices.mobileL} {
    font-size: 20px;
  }
`;

import React from "react";

import { ControllerFieldState } from "react-hook-form";
import styled from "styled-components";

import { Check, Close, devices } from "assets";
import { COLORS, TYPOGRAPHY } from "styles";

const Input: React.FC<InputProps> = (props) => {
  const { error, isTouched } = props.fieldState;
  return (
    <Root>
      <InputInsert isTouched={isTouched} isError={!!error} {...props} />
      {isTouched && (
        <Icon>
          {error ? (
            <Close strokeColor={COLORS.red3} />
          ) : (
            <Check strokeColor={COLORS.green} />
          )}
        </Icon>
      )}
      <ErrorMessage>{error?.message || ""}</ErrorMessage>
    </Root>
  );
};

export default Input;

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  fieldState: ControllerFieldState;
}
interface InputInsertProps {
  isTouched: boolean;
  isError: boolean;
}

const Icon = styled.div`
  position: absolute;
  right: -7%;
  top: 27%;
  @media ${devices.tablet} {
    right: -4%;
    top: 24%;
  }
  @media ${devices.mobileL} {
    right: -4%;
    top: 16%;
  }
`;

const InputInsert = styled.input<InputInsertProps>`
  background: ${({ theme }) => theme.colors.color1};
  width: 100%;
  height: 68px;
  padding-right: 40px;
  padding-left: 23px;
  border-radius: 6px;
  ${TYPOGRAPHY.paragraph}
  caret-color: ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.color7};
  border: none;
  ::placeholder {
    color: ${({ theme }) => theme.colors.color5};
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 18px;
  }
  :disabled {
    background-color: ${({ theme }) => theme.colors.color3};
  }
  outline: 1px solid;
  outline-color: ${({ isTouched, isError, theme: { colors } }) => {
    if (isTouched) {
      return isError ? colors.red3 : colors.green;
    }
    return "inherit";
  }};
  @media ${devices.tablet} {
    padding-right: 20px;
    padding-left: 13px;
    height: 58px;
  }
  @media ${devices.mobileL} {
    padding-right: 10px;
    padding-left: 8px;
    height: 42px;
  }
`;
const Root = styled.div`
  position: relative;
  width: 100%;
  max-height: max-content;
  display: flex;
  flex-direction: column;
`;
const ErrorMessage = styled.div`
  color: ${({ theme }) => theme.colors.red3};
  font-size: 10px;
  display: flex;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  height: 20px;
`;

import { css, keyframes } from "styled-components";

import { devices } from "assets";

const renderPrimaryButton = css`
  ${({ theme: { colors } }) => {
    return css`
      background-color: ${colors.primary};
      color: ${colors.color1};
      box-shadow: 0px 10px 28px rgba(252, 88, 66, 0.2);
      :hover {
        background-color: ${colors.red4};
      }
      :focus {
        outline: 5px rgb(252, 88, 66, 0.4) solid;
      }
      :disabled {
        opacity: 0.4;
      }
    `;
  }}
`;
const renderSecondaryButton = css<{ textColor?: string }>`
  ${({ theme: { colors } }) => {
    return css`
      background-color: ${colors.color1};
      color: ${colors.primary};
      box-shadow: 0px 10px 28px rgba(252, 88, 66, 0.2);
      :hover {
        color: ${colors.red4};
      }
      :focus {
        outline: 5px rgb(255, 255, 255, 0.4) solid;
      }
      :disabled {
        opacity: 0.4;
      }
    `;
  }}
`;
const renderTextButton = css`
  ${({ theme: { colors } }) => {
    return css`
      background-color: transparent;
      color: ${colors.primary};
      width: max-content;
      margin: 5px;
      @media ${devices.tablet} {
        margin: 6px;
      }
      height: max-content;
      :hover {
        color: ${colors.red4};
      }
      :disabled {
        opacity: 0.4;
      }
    `;
  }}
`;
const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;
export const THEMES = {
  primary: renderPrimaryButton,
  secondary: renderSecondaryButton,
  text: renderTextButton,
};
export const LOADER = css`
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);
`;

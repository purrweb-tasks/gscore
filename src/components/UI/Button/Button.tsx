import React from "react";

import styled from "styled-components";

import { devices, Loader } from "assets";
import { COLORS, TYPOGRAPHY } from "styles";

import { LOADER, THEMES } from "./utils/themes";

const Button: React.FC<ButtonProps> = ({
  variant,
  isLoading,
  isUnderline,
  textColor,
  ...props
}) => {
  return (
    <Root
      textColor={textColor}
      variant={variant}
      isLoading={isLoading}
      {...props}
    >
      <Load isLoading={isLoading}>
        {isLoading ? (
          <Loader
            strokeColor={variant === "primary" ? COLORS.color1 : COLORS.primary}
          />
        ) : (
          props.value
        )}
      </Load>
    </Root>
  );
};

export default Button;

interface ButtonProps
  extends VariantButtonProps,
    React.ButtonHTMLAttributes<HTMLButtonElement> {}

interface VariantButtonProps {
  variant: "primary" | "secondary" | "text";
  isLoading?: boolean;
  isUnderline?: boolean;
  textColor?: string;
}
const Root = styled.button<VariantButtonProps>`
  border-radius: 4px;
  width: 100%;
  height: 68px;
  ${TYPOGRAPHY.textBold};
  border: none;
  text-align: center;
  text-decoration: ${({ isUnderline }) => (isUnderline ? "underline" : "none")};
  cursor: pointer;
  :focus {
    outline: none;
  }
  @media ${devices.desktop} {
    height: 58px;
  }
  @media ${devices.tablet} {
    height: 48px;
  }
  @media ${devices.mobileL} {
    height: 38px;
    font-size: 14px;
  }
  ${({ variant }) => THEMES[variant]};
  ${({ textColor }) => (textColor ? `color: ${textColor}` : null)}
`;
const Load = styled.div<{ isLoading?: boolean }>`
  ${({ isLoading }) => (isLoading ? LOADER : "")}
`;

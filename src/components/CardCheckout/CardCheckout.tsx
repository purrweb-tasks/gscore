import styled from "styled-components";

import { Basket } from "assets";
import { TYPOGRAPHY } from "styles";
import { Product } from "types";

const CardCheckout: React.FC<CardCheckoutProps> = ({ product }) => {
  const { name, prices } = product;
  return (
    <Root>
      <CardHeader>
        <PackageHeader>Package name</PackageHeader>
        <PriceHeader>Price</PriceHeader>
      </CardHeader>
      <Border />
      <CardContent>
        <PackageContent>{name} license</PackageContent>
        <PriceContent>
          $ {prices[0].price} <Basket />
        </PriceContent>
      </CardContent>
    </Root>
  );
};

export default CardCheckout;

interface CardCheckoutProps {
  product: Product;
}

const Root = styled.div`
  border-radius: 12px;
  height: 232px;
  background-color: ${({ theme }) => theme.colors.color9};
  width: 100%;
  color: ${({ theme }) => theme.colors.color1};
`;
const CardHeader = styled.div`
  ${TYPOGRAPHY.header}
  font-weight: 700;
  font-size: 24px;
  height: 50%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 70px 0 20px;
`;
const PackageHeader = styled.div``;
const PriceHeader = styled.div``;
const CardContent = styled.div`
  ${TYPOGRAPHY.headerS};
  height: 50%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 50px 0 20px;
  font-weight: 400;
`;
const PackageContent = styled.div``;
const PriceContent = styled.div``;
const Border = styled.div`
  background-color: ${({ theme }) => theme.colors.color5};
  height: 1px;
  width: 100%;
`;

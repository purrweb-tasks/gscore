import { useState } from "react";

import { useRouter } from "next/router";
import styled from "styled-components";

import {
  ChevronDown,
  Close,
  devices,
  FullLogo,
  Logout,
  Menu,
  Settings,
} from "assets";
import { useAppSelector, selectors, useAppDispatch, actions } from "store";
import { COLORS, TYPOGRAPHY } from "styles";

const UserMenu: React.FC<UserMenuProps> = ({}) => {
  const router = useRouter();
  const user = useAppSelector(selectors.user.getUser);
  const dispatch = useAppDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenBigMobileMenu, setIsOpenBigMobileMenu] = useState(false);
  return (
    <Root>
      <Subscriptions onClick={() => router.push("subscriptions")}>
        My subscriptions
      </Subscriptions>
      <User
        onClick={() => router.push("settings")}
        onMouseOver={() => {
          setIsOpen(true);
        }}
        onMouseOut={() => {
          setIsOpen(false);
        }}
      >
        {user?.username}
        <CheckIcon isOpen={isOpen}>
          <ChevronDown />
        </CheckIcon>
      </User>
      <UserMobile onClick={() => setIsOpenBigMobileMenu(true)}>
        <Menu />
      </UserMobile>
      <MobileOpenMenu isOpen={isOpenBigMobileMenu}>
        <MobileMenuHeader>
          <IconClose onClick={() => setIsOpenBigMobileMenu(false)}>
            <Close width={30} height={30} />
          </IconClose>
          <Logo>
            <FullLogo />
          </Logo>
        </MobileMenuHeader>
        <SubscriptionsMobile
          onClick={() => {
            router.push("subscriptions");
            setIsOpenBigMobileMenu(false);
          }}
        >
          My subscriptions
        </SubscriptionsMobile>
        <Border />
        <UserName onClick={() => setIsOpen(!isOpen)}>
          {user?.username}
          <CheckIcon isOpen={isOpen}>
            <ChevronDown />
          </CheckIcon>
        </UserName>
        <OpenMobileUserMenu onClick={() => setIsOpen(!isOpen)} isOpen={isOpen}>
          <SettingsItem
            onClick={() => {
              router.push("/settings");
              setIsOpenBigMobileMenu(false);
            }}
          >
            <SettingsIcon>
              <Settings strokeColor={COLORS.color5} />
            </SettingsIcon>
            Settings
          </SettingsItem>
          <LogoutItem
            onClick={() => {
              dispatch(actions.auth.logout());
              router.push("/");
            }}
          >
            <LogOutIcon>
              <Logout strokeColor={COLORS.color5} />
            </LogOutIcon>
            Logout
          </LogoutItem>
        </OpenMobileUserMenu>
        <Border />
      </MobileOpenMenu>
      <OpenMenu
        onMouseOver={() => {
          setIsOpen(true);
        }}
        onMouseOut={() => {
          setIsOpen(false);
        }}
        isOpen={isOpen}
      >
        <SettingsItem onClick={() => router.push("/settings")}>
          <SettingsIcon>
            <Settings />
          </SettingsIcon>
          Settings
        </SettingsItem>
        <LogoutItem
          onClick={() => {
            dispatch(actions.auth.logout());
            router.push("/");
          }}
        >
          <LogOutIcon>
            <Logout />
          </LogOutIcon>
          Logout
        </LogoutItem>
      </OpenMenu>
      <Background isDisabled={isOpenBigMobileMenu} />
    </Root>
  );
};

export default UserMenu;

interface UserMenuProps {}
const Root = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  ${TYPOGRAPHY.headerS};
  font-size: 20px;
`;
const Background = styled.div<{ isDisabled: boolean }>`
  background-color: rgb(39, 39, 39, 0.5);
  width: 100vw;
  height: 500vh;
  position: absolute;
  z-index: 9;
  display: none;
  right: 0;
  top: -30px;
  @media ${devices.tablet} {
    display: ${({ isDisabled }) => (isDisabled ? "block" : "none")};
  }
`;
const UserMobile = styled.button`
  padding: 0;
  display: none;
  border: none;
  padding-right: 40px;
  background-color: transparent;
  cursor: pointer;
  @media ${devices.tablet} {
    display: block;
  }
`;
const MobileOpenMenu = styled.div<{ isOpen: boolean }>`
  background-color: ${({ theme }) => theme.colors.color9};
  height: 101vh;
  width: 60vw;
  right: 0;
  top: -5px;
  z-index: 10;
  padding: 30px;
  display: none;
  overflow: hidden;
  position: fixed;
  @media ${devices.tablet} {
    display: ${({ isOpen }) => (isOpen ? "block" : "none")};
  }
  @media ${devices.mobileL} {
    width: 70vw;
  }
`;
const MobileMenuHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 30px;
  @media ${devices.mobileS} {
    flex-direction: column;
  }
`;
const IconClose = styled.button`
  margin-right: 10px;
  border: none;
  background-color: transparent;
  cursor: pointer;
  padding: 0;
`;
const Logo = styled.div``;
const SubscriptionsMobile = styled.button`
  padding-bottom: 30px;
  border: none;
  background-color: transparent;
  cursor: pointer;
  ${TYPOGRAPHY.headerS};
  font-weight: 500;
  font-size: 20px;
  color: ${({ theme }) => theme.colors.color1};
`;
const Border = styled.div`
  background-color: ${({ theme }) => theme.colors.color7};
  height: 1px;
  width: 90%;
`;
const UserName = styled.button`
  border: none;
  background-color: transparent;
  cursor: pointer;
  padding-top: 20px;
  padding-bottom: 20px;
  display: flex;
  width: 100%;
  justify-content: space-between;
  ${TYPOGRAPHY.headerS};
  font-weight: 500;
  font-size: 20px;
  color: ${({ theme }) => theme.colors.color1};
`;
const OpenMobileUserMenu = styled.div<{ isOpen: boolean }>`
  display: ${({ isOpen }) => (isOpen ? "block" : "none")};
`;
const Subscriptions = styled.button`
  margin-right: 40px;
  ${TYPOGRAPHY.headerS};
  font-size: 20px;
  padding-bottom: 20px;
  padding: 0;
  color: ${({ theme }) => theme.colors.color1};
  border: none;
  background-color: transparent;
  cursor: pointer;
  @media ${devices.tablet} {
    display: none;
  }
`;
const User = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  padding-right: 40px;
  @media ${devices.tablet} {
    display: none;
  }
`;
const CheckIcon = styled.div<{ isOpen: boolean }>`
  transform: ${({ isOpen }) => (isOpen ? "rotate(180deg)" : "rotate(0deg)")};
  margin-left: 10px;
`;
const OpenMenu = styled.div<{ isOpen: boolean }>`
  display: ${({ isOpen }) => (isOpen ? "flex" : "none")};
  background-color: ${({ theme }) => theme.colors.color9};
  width: max-content;
  height: max-content;
  border-radius: 12px;
  padding: 20px;
  flex-direction: column;
  position: absolute;
  top: 100%;
  left: 40%;
  color: ${({ theme }) => theme.colors.color1};
  @media ${devices.tablet} {
    display: none;
  }
`;
const SettingsItem = styled.button`
  ${TYPOGRAPHY.headerS};
  font-size: 20px;
  margin-bottom: 20px;
  padding: 0;
  color: ${({ theme }) => theme.colors.color1};
  border: none;
  background-color: transparent;
  display: flex;
  cursor: pointer;
  @media ${devices.tablet} {
    padding-top: 20px;
    color: ${({ theme }) => theme.colors.color5};
  }
`;
const LogoutItem = styled.button`
  ${TYPOGRAPHY.headerS};
  font-size: 20px;
  display: flex;
  color: ${({ theme }) => theme.colors.color1};
  padding: 0;
  border: none;
  background-color: transparent;
  cursor: pointer;
  @media ${devices.tablet} {
    padding-bottom: 20px;
    color: ${({ theme }) => theme.colors.color5};
  }
`;
const LogOutIcon = styled.div`
  padding-right: 10px;
`;
const SettingsIcon = styled.div`
  padding-right: 10px;
`;

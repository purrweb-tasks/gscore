import { useRouter } from "next/router";
import styled from "styled-components";

import { Check, devices } from "assets";
import { Button } from "components";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { COLORS, TYPOGRAPHY } from "styles";
import { Product } from "types";

const BuyLicenseCard: React.FC<BuyLicenseCardProps> = ({ product }) => {
  const { id, prices, sitesCount } = product;
  const router = useRouter();
  const currentSubscribeId = useAppSelector(
    selectors.payment.getUpdatedSubcribe
  );
  const currentSubscribe = useAppSelector(
    selectors.subscribes.getSubscribeById(currentSubscribeId)
  );
  const authToken = useAppSelector(selectors.auth.getToken);
  const dispatch = useAppDispatch();
  const handleCurrentSubscribe = async () => {
    if (authToken) {
      if (currentSubscribeId) {
        await dispatch(
          actions.subscribes.changeSubscribeThunk({
            productId: id,
            subscribeId: currentSubscribeId,
          })
        );
        if (currentSubscribe?.productId && currentSubscribe.productId <= id) {
          dispatch(
            actions.payment.setUpdatedSubscribe({ updatedSubcribeId: null })
          );
        }
        return router.push("/subscriptions");
      }
      dispatch(actions.payment.setCurrentProductId({ currentProductId: id }));
      return router.push("/subscribe");
    }
    return router.push("/auth");
  };

  return (
    <Root isBought={id === 2}>
      <Header>
        <Cost>${prices[0].price}</Cost>
        <Lable>{sitesCount} site license</Lable>
        <LicenseParagraph isBought={id === 2}>
          Get the advanced WordPress plugin that optimizes content with GSC
          keywords at one low annual price
        </LicenseParagraph>
      </Header>
      <Border isBought={id === 2} />
      <LisenseList>
        <ListOne>
          <Icon>
            <Check
              width={18}
              height={16}
              strokeColor={id === 2 ? COLORS.primary : COLORS.black}
            />
          </Icon>
          <ListOneText>All features for {sitesCount} site</ListOneText>
        </ListOne>
        <ListOne>
          <Icon>
            <Check
              width={18}
              height={16}
              strokeColor={id === 2 ? COLORS.primary : COLORS.black}
            />
          </Icon>
          <ListOneText>Special introductory pricing</ListOneText>
        </ListOne>
        <ListOne>
          <Icon>
            <Check
              width={18}
              height={16}
              strokeColor={id === 2 ? COLORS.primary : COLORS.black}
            />
          </Icon>
          <ListOneText>Unlimited Pages and Keywords</ListOneText>
        </ListOne>
        <ListOne>
          <Icon>
            <Check
              width={18}
              height={16}
              strokeColor={id === 2 ? COLORS.primary : COLORS.black}
            />
          </Icon>
          <ListOneText>Billed annually</ListOneText>
        </ListOne>
      </LisenseList>
      <ButtonBuy>
        <Button
          textColor={id === 2 ? undefined : COLORS.black}
          variant="secondary"
          value="Get Gscore"
          onClick={handleCurrentSubscribe}
        />
      </ButtonBuy>
    </Root>
  );
};

export default BuyLicenseCard;
interface BuyLicenseCardProps {
  isBought?: boolean;
  product: Product;
}

const Root = styled.div<{ isBought?: boolean }>`
  width: 400px;
  height: 550px;
  position: relative;
  background-color: ${({ isBought, theme }) =>
    isBought ? theme.colors.primary : theme.colors.color9};
  margin-top: ${({ isBought }) => (isBought ? "-50px" : "0px")};
  border-radius: 12px;
  padding: 40px 40px 20px 40px;
  @media ${devices.laptopM} {
    padding: 20px 20px 10px 20px;
    height: 520px;
  }
  @media ${devices.laptop} {
    margin-bottom: 50px;
    margin-top: 0;
  }
  @media ${devices.tablet} {
    margin-bottom: 50px;
  }
  @media ${devices.mobileL} {
    padding: 30px 30px 10px 30px;
    height: 520px;
    width: 375px;
  }
  @media ${devices.mobileM} {
    padding: 20px 20px 10px 20px;
    width: 325px;
  }
  @media ${devices.mobileS} {
    padding: 10px 10px 10px 10px;
    width: 100vw;
  }
`;
const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Cost = styled.div`
  ${TYPOGRAPHY.headerL};
  color: ${({ theme }) => theme.colors.color1};
  padding-bottom: 10px;
`;
const Lable = styled.div`
  padding-bottom: 10px;
  ${TYPOGRAPHY.headerS};
  color: ${({ theme }) => theme.colors.color1};
`;
const LicenseParagraph = styled.div<{ isBought: boolean }>`
  ${TYPOGRAPHY.paragraph};
  font-weight: 300;
  line-height: 30px;
  color: ${({ theme, isBought }) =>
    isBought ? theme.colors.color1 : theme.colors.color4};
  text-align: center;
`;
const Border = styled.div<{ isBought: boolean }>`
  width: 94%;
  margin-left: 3%;
  height: 1px;
  margin-top: 30px;
  margin-bottom: 30px;
  background-color: ${({ theme, isBought }) =>
    isBought ? theme.colors.color1 : theme.colors.color4};
`;
const LisenseList = styled.div``;
const ListOne = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 10px;
  ${TYPOGRAPHY.paragraph};
`;
const Icon = styled.div`
  width: max-content;
  height: max-content;
  border-radius: 20px;
  padding-top: 4px;
  padding-right: 4px;
  padding-left: 3px;
  background-color: ${({ theme }) => theme.colors.color1};
`;
const ListOneText = styled.div`
  color: ${({ theme }) => theme.colors.color1};
  padding-left: 10px;
  ${TYPOGRAPHY.paragraphBold};
`;
const ButtonBuy = styled.div`
  padding-top: 20px;
  display: flex;
  justify-content: center;
`;

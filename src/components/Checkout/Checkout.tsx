import React from "react";

import styled from "styled-components";

import { Button, CardCheckout } from "components";
import { TYPOGRAPHY } from "styles";
import { Product } from "types";

const Checkout: React.FC<CheckoutProps> = ({ product, buySubscribe }) => {
  const { prices } = product;
  return (
    <Root>
      <Header>Checkout</Header>
      <CardCheckout product={product} />
      <Total>
        <TotalHeader>Total:</TotalHeader>
        <TotalCost>$ {prices[0].price}</TotalCost>
      </Total>
      <Button variant="primary" value="Purchase" onClick={buySubscribe} />
    </Root>
  );
};

export default Checkout;

interface CheckoutProps {
  product: Product;
  buySubscribe: () => void;
}

const Root = styled.div`
  height: 800px;
  width: 100%;
`;
const Header = styled.div`
  ${TYPOGRAPHY.headerL};
  color: ${({ theme }) => theme.colors.color1};
  padding: 50px 0 30px 0;
`;
const Total = styled.div`
  ${TYPOGRAPHY.headerM}
  padding: 20px 0 50px 0;
  display: flex;
  width: 100%;
  justify-content: space-between;
  color: ${({ theme }) => theme.colors.color1};
`;
const TotalHeader = styled.div``;
const TotalCost = styled.div``;

import styled from "styled-components";

import { devices } from "assets";
import { TYPOGRAPHY } from "styles";

const CrossBarTab: React.FC<CrossBarTabProps> = ({
  activeTab,
  tabs,
  setActiveTab,
}) => {
  return (
    <Root>
      {tabs.map((tab, index) => (
        <Stage
          onClick={() => setActiveTab(tab)}
          key={index}
          isActiveTab={activeTab === tab}
        >
          {tab}
        </Stage>
      ))}
      <LongBorder />
    </Root>
  );
};

export default CrossBarTab;

interface CrossBarTabProps {
  tabs: string[];
  activeTab: string;
  setActiveTab: (newActiveTab: string) => void;
}

const Root = styled.div`
  display: flex;
  @media ${devices.desktop} {
    font-size: 16px;
  }
  @media ${devices.tablet} {
    font-size: 14px;
  }
  @media ${devices.mobileL} {
    font-size: 12px;
  }
`;
const Stage = styled.button<{ isActiveTab: boolean }>`
  padding-left: 20px;
  padding-bottom: 10px;
  background: inherit;
  border: none;
  ${TYPOGRAPHY.header}
  cursor: pointer;
  border-bottom: 2px solid;
  color: ${({ theme, isActiveTab }) =>
    isActiveTab ? theme.colors.primary : theme.colors.color6};
  @media ${devices.tablet} {
    font-size: 16px;
  }
  @media ${devices.mobileL} {
    font-size: 16px;
  }
  @media ${devices.mobileL} {
    font-size: 12px;
  }
`;
const LongBorder = styled.div`
  margin-top: 33px;
  height: 2px;
  width: 50vw;
  background-color: ${({ theme }) => theme.colors.color6};
  @media ${devices.laptop} {
    width: 30vw;
  }
  @media ${devices.tablet} {
    margin-top: 34px;
    width: 15vw;
  }
  @media ${devices.mobileL} {
    width: 10vw;
  }
`;

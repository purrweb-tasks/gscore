import React from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import {
  Controller,
  ErrorOption,
  SubmitHandler,
  useForm,
} from "react-hook-form";
import styled from "styled-components";

import { devices } from "assets";
import { Button, Input } from "components";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";
import { validators } from "utils";

interface FormInputs {
  currentPassword: string;
  newPassword: string;
}

const ChangePassword: React.FC<ChangePasswordProps> = ({}) => {
  const isLoading = useAppSelector(selectors.settings.getIsLoading);
  const dispatch = useAppDispatch();
  const {
    handleSubmit,
    setError,
    reset,
    control,
    formState: { isValid },
  } = useForm<FormInputs>({
    mode: "all",
    resolver: yupResolver(validators.changePasswordValidator),
    defaultValues: { currentPassword: "", newPassword: "" },
  });
  const onSubmit: SubmitHandler<FormInputs> = async (data) => {
    const { payload, meta } = await dispatch(
      actions.settings.updatePasswordThunk(data)
    );
    if (meta.requestStatus === "rejected") {
      return setError("currentPassword", payload as ErrorOption);
    }
    reset();
  };
  return (
    <Root>
      <Header>Change password</Header>
      <ChangePasswordForm onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name="currentPassword"
          render={({ field: { onChange, onBlur, value }, fieldState }) => (
            <Input
              value={value}
              type="password"
              placeholder="Current Password"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <Controller
          control={control}
          name="newPassword"
          render={({ field: { onChange, onBlur, value }, fieldState }) => (
            <Input
              type="password"
              value={value}
              placeholder="New Password"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <ChangeButton>
          <Button
            disabled={!isValid || isLoading}
            variant="primary"
            value="Save"
            isLoading={isLoading}
          />
        </ChangeButton>
      </ChangePasswordForm>
    </Root>
  );
};

export default ChangePassword;

interface ChangePasswordProps {}
const Root = styled.div`
  height: 800px;
  width: 90%;
`;
const Header = styled.div`
  ${TYPOGRAPHY.headerM}
  padding-top: 60px;
  padding-bottom: 30px;
  color: ${({ theme }) => theme.colors.color1};
`;
const ChangePasswordForm = styled.form`
  width: 100%;
  @media ${devices.tablet} {
    width: 150%;
  }
  @media ${devices.mobileL} {
    width: 170%;
  }
`;
const ChangeButton = styled.div`
  width: 160px;
`;

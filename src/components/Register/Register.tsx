import React from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import {
  Controller,
  ErrorOption,
  useForm,
  SubmitHandler,
} from "react-hook-form";
import styled from "styled-components";

import { Button, Input } from "components";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";
import { validators } from "utils";

interface FormInputs {
  username: string;
  email: string;
  password: string;
}

const Register: React.FC<RegisterProps> = ({ setNextStage }) => {
  const isLoading = useAppSelector(selectors.auth.getIsLoading);
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectors.user.getUser);
  const {
    handleSubmit,
    control,
    setError,
    formState: { isValid },
  } = useForm<FormInputs>({
    mode: "all",
    resolver: yupResolver(validators.registerValidator),
  });
  const onSubmit: SubmitHandler<FormInputs> = async (data) => {
    const { payload, meta } = await dispatch(actions.auth.signUpThunk(data));
    if (meta.requestStatus === "rejected") {
      return setError("email", payload as ErrorOption);
    }
    if (!isLoading) {
      setNextStage(2);
    }
  };
  return (
    <Root>
      <Header>Create account</Header>
      <ByEmail>
        You need to enter your name and email. We will send you a temporary
        password by email
      </ByEmail>
      <RegisterForm onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name="username"
          render={({ field: { onChange, onBlur }, fieldState }) => (
            <Input
              type="text"
              placeholder="Username"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <Controller
          control={control}
          name="email"
          render={({ field: { onChange, onBlur }, fieldState }) => (
            <Input
              type="email"
              placeholder="Email"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <Controller
          control={control}
          name="password"
          render={({ field: { onChange, onBlur }, fieldState }) => (
            <Input
              type="password"
              placeholder="Password"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <RegisterButton>
          <Button
            disabled={!isValid || isLoading}
            isLoading={isLoading}
            variant="primary"
            value="Send password"
          />
        </RegisterButton>

        <NextStep>
          <NextStepText>Have an account? </NextStepText>
          <Button
            variant="text"
            value="Go to the next step"
            onClick={() => setNextStage(2)}
          />
        </NextStep>
      </RegisterForm>
    </Root>
  );
};

export default Register;

interface RegisterProps {
  setNextStage: (nextStage: number) => void;
}

const Root = styled.div`
  height: 800px;
  width: 90%;
`;
const NextStep = styled.div`
  display: flex;
  align-items: center;
`;
const NextStepText = styled.div`
  color: ${({ theme }) => theme.colors.color1};
  ${TYPOGRAPHY.link};
`;
const ByEmail = styled.p`
  ${TYPOGRAPHY.paragraph};
  font-size: 14px;
  padding-bottom: 30px;
  line-height: 24px;
  color: ${({ theme }) => theme.colors.color1};
`;
const Header = styled.div`
  ${TYPOGRAPHY.headerL}
  padding-top: 60px;
  padding-bottom: 10px;
  color: ${({ theme }) => theme.colors.color1};
`;
const RegisterForm = styled.form`
  width: 100%;
`;
const RegisterButton = styled.div`
  width: 200px;
`;

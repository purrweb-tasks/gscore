import styled from "styled-components";

import { devices } from "assets";
import { TYPOGRAPHY } from "styles";

const Tab: React.FC<TabProps> = ({ step, tabs }) => {
  return (
    <Root>
      {tabs.map((tab, index) => (
        <Stage key={index}>
          <Header>{tab}</Header>
          <Border isActiveTab={step <= index}></Border>
        </Stage>
      ))}
    </Root>
  );
};

export default Tab;

interface TabProps {
  tabs: string[];
  step: number;
}
const Root = styled.div`
  color: ${({ theme }) => theme.colors.color1};
  display: flex;
  @media ${devices.tablet} {
    font-size: 16px;
  }
  @media ${devices.mobileL} {
    font-size: 14px;
  }
  @media ${devices.mobileS} {
    font-size: 12px;
  }
  @media ${devices.mobileM} {
    font-size: 10px;
  }
`;
const Stage = styled.span`
  padding-bottom: 3px;
`;
const Header = styled.span`
  ${TYPOGRAPHY.headerM}
  @media ${devices.tablet} {
    font-size: 16px;
  }
  @media ${devices.mobileL} {
    font-size: 14px;
  }
`;
const Border = styled.div<{ isActiveTab: boolean }>`
  margin-top: 10px;
  margin-right: 15px;
  height: 8px;
  width: 280px;
  border-radius: 8px;
  @media ${devices.desktop} {
    width: 220px;
  }
  @media ${devices.laptop} {
    width: 200px;
  }
  @media ${devices.tablet} {
    width: 120px;
    margin-top: 0px;
  }
  @media ${devices.mobileL} {
    width: 100px;
    margin-top: 5px;
  }
  @media ${devices.mobileM} {
    width: 80px;
    margin-top: 2px;
  }
  background-color: ${({ theme, isActiveTab }) =>
    isActiveTab ? theme.colors.color6 : theme.colors.primary};
`;

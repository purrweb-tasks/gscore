import React from "react";

import styled from "styled-components";

import { Copy, devices } from "assets";
import { Button, Checkbox, Status } from "components";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";
import { Code } from "types";

const CardLink: React.FC<CardLinkProps> = ({
  cardCode,
  selectDomain,
  checked,
  isActivating,
}) => {
  const dispatch = useAppDispatch();
  const isLoading = useAppSelector(selectors.codes.getIsLoading);
  const copyText = (text: string) => {
    navigator.clipboard.writeText(text);
  };
  const { code, status, origin } = cardCode;
  const isActive = status !== "INACTIVE";
  const isActiveButton = !isActive;
  const handleActivateCode = async () => {
    dispatch(actions.codes.setActiveCode({ activeCode: cardCode }));
    await dispatch(actions.codes.activeCodeThunk({ code }));
    await dispatch(actions.subscribes.getSubscribesThunk());
  };
  return (
    <Root>
      <Check isActive={isActive}>
        <Checkbox onChange={() => selectDomain(checked)} checked={checked} />
      </Check>
      <AllLinks>
        <LicenseCode isActive={isActive}>
          <LicenseHead>License code</LicenseHead>
          <License isActive={isActive}>
            <LicenseLink>{code}</LicenseLink>
            <CopyButton onClick={() => copyText(code)}>
              <Copy />
            </CopyButton>
          </License>
        </LicenseCode>
        <Domain>
          <DomainHead>Domain</DomainHead>
          <DomainLink isActive={isActive}>{origin}</DomainLink>
        </Domain>
        <ActivateStatus>
          <ActivateButton isActiveButton={!isActiveButton}>
            <Button
              onClick={handleActivateCode}
              isLoading={isActivating}
              variant="secondary"
              value="Activate"
              disabled={isLoading}
            />
          </ActivateButton>
          <StatusState>
            <StatusHead>Status</StatusHead>
            <State isActive={isActive}>
              <Status status={status} />
            </State>
          </StatusState>
        </ActivateStatus>
      </AllLinks>
    </Root>
  );
};

export default CardLink;

interface CardLinkProps {
  cardCode: Code;
  selectDomain: (checked: boolean) => void;
  checked: boolean;
  isActivating: boolean;
}
interface StateProps {
  isActive?: boolean;
}
const AllLinks = styled.div`
  display: flex;
  justify-content: space-between;
  margin-left: 50px;
  width: 100%;
  @media ${devices.tablet} {
    flex-direction: column;
    align-items: center;
    margin-left: 0px;
  }
`;
const Check = styled.div<StateProps>`
  padding-top: 20px;
  @media ${devices.tablet} {
    position: absolute;
    left: 6%;
    top: ${({ isActive }) => (isActive ? "2%" : "5%")};
  }
  @media ${devices.mobileL} {
    top: ${({ isActive }) => (isActive ? "4%" : "3%")};
  }
  @media ${devices.mobileM} {
    top: ${({ isActive }) => (isActive ? "4%" : "3%")};
  }
`;
const Root = styled.div`
  background-color: ${({ theme }) => theme.colors.color9};
  height: 130px;
  position: relative;
  display: flex;
  width: 100%;
  padding: 30px;
  border-radius: 12px;
  align-items: center;
  @media ${devices.laptop} {
    padding: 20px;
  }
  @media ${devices.tablet} {
    flex-direction: column;
    height: max-content;
    width: max-content;
  }
  @media ${devices.mobileL} {
    padding: 10px;
  }
  @media ${devices.mobileM} {
    padding-bottom: 15px;
  }
`;
const License = styled.div<StateProps>`
  background-color: ${({ theme }) => theme.colors.color7};
  padding: 24px;
  border-radius: 6px;
  height: 68px;
  width: 20vw;
  display: flex;
  align-items: center;
  @media ${devices.laptop} {
    width: 15vw;
    padding: 14px;
  }
  @media ${devices.tablet} {
    width: 270px;
  }
  @media ${devices.mobileL} {
    height: 48px;
    width: 260px;
  }
  @media ${devices.mobileM} {
    width: 220px;
  }
  @media ${devices.mobileS} {
    width: 200px;
  }
`;
const LicenseCode = styled.div<StateProps>`
  @media ${devices.tablet} {
    order: 2;
  }
`;
const LicenseHead = styled.div`
  color: ${({ theme }) => theme.colors.color5};
  padding-bottom: 10px;
  @media ${devices.tablet} {
    display: block;
  }
`;
const LicenseLink = styled.div`
  ${TYPOGRAPHY.link}
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${({ theme }) => theme.colors.color5};
`;
const DomainLink = styled.div<StateProps>`
  padding: 24px;
  color: ${({ theme }) => theme.colors.color5};
  background-color: ${({ theme }) => theme.colors.color7};
  width: max-content;
  max-width: 40vw;
  min-width: 30vw;
  height: 68px;
  white-space: nowrap;
  overflow: hidden;
  ${TYPOGRAPHY.link};
  ${({ isActive }) => (isActive ? `width: 624px` : null)};
  text-overflow: ellipsis;
  border-radius: 6px;
  @media ${devices.laptopM} {
    ${({ isActive }) => (isActive ? `width: 35vw` : null)};
    min-width: 25vw;
  }
  @media ${devices.laptop} {
    min-width: 20vw;
    max-width: 30;
  }
  @media ${devices.tablet} {
    max-width: 270px;
    min-width: 270px;
    order: 3;
  }
  @media ${devices.mobileL} {
    height: 48px;
    max-width: 260px;
    min-width: 260px;
    padding: 14px;
  }
  @media ${devices.mobileM} {
    max-width: 220px;
    min-width: 220px;
  }
  @media ${devices.mobileS} {
    max-width: 200px;
    min-width: 200px;
  }
`;
const ActivateStatus = styled.div`
  display: flex;
  @media ${devices.tablet} {
    order: 1;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding-bottom: 10px;
    padding-top: 10px;
  }
`;
const CopyButton = styled.button`
  padding: 0;
  border: none;
  font: inherit;
  color: inherit;
  background-color: transparent;
  cursor: pointer;
  padding-left: 30px;
`;
const Domain = styled.div`
  @media ${devices.tablet} {
    order: 3;
    margin-top: 20px;
  }
`;
const DomainHead = styled.div<StateProps>`
  color: ${({ theme }) => theme.colors.color5};
  padding-bottom: 10px;
  @media ${devices.tablet} {
    display: block;
  }
`;
const StatusState = styled.div`
  padding-left: 30px;
  @media ${devices.tablet} {
    display: flex;
    height: 100%;
    align-items: center;
  }
`;
const StatusHead = styled.div`
  color: ${({ theme }) => theme.colors.color5};
  margin-bottom: 20px;
  @media ${devices.tablet} {
    display: none;
  }
`;
const State = styled.div<StateProps>`
  background: none;
  margin-bottom: 30px;
  padding-top: 5px;
  padding-right: ${({ isActive }) => (isActive ? "30px" : "0px")};
  @media ${devices.laptop} {
    padding-right: ${({ isActive }) => (isActive ? "20px" : "0px")};
  }
  @media ${devices.tablet} {
    margin-bottom: 0px;
    padding-left: 15px;
    padding-top: 0px;
  }
  @media ${devices.mobileL} {
    padding-top: ${({ isActive }) => (isActive ? "10px" : "0px")};
  }
  @media ${devices.mobileM} {
  }
`;
const ActivateButton = styled.div<{ isActiveButton: boolean }>`
  padding-top: 36px;
  margin-left: 20px;
  width: 111px;
  display: ${({ isActiveButton }) => (isActiveButton ? "none" : "block")};
  @media ${devices.tablet} {
    order: 2;
    padding-top: 0;
  }
  @media ${devices.mobileL} {
    padding-left: 10px;
    width: 80px;
    margin-left: 30px;
  }
  @media ${devices.mobileM} {
    width: 80px;
  }
`;

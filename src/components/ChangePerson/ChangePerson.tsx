import React from "react";

import { yupResolver } from "@hookform/resolvers/yup";
import {
  Controller,
  ErrorOption,
  SubmitHandler,
  useForm,
} from "react-hook-form";
import styled from "styled-components";

import { devices } from "assets";
import { Button, Input } from "components";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";
import { validators } from "utils";

interface FormInputs {
  username: string;
  email: string;
}

const ChangePerson: React.FC<ChangePersonProps> = () => {
  const isLoading = useAppSelector(selectors.settings.getIsLoading);
  const dispatch = useAppDispatch();
  const {
    handleSubmit,
    setError,
    reset,
    control,
    formState: { isValid },
  } = useForm<FormInputs>({
    mode: "all",
    resolver: yupResolver(validators.changePersonValidator),
    defaultValues: { username: "", email: "" },
  });
  const onSubmit: SubmitHandler<FormInputs> = async (data) => {
    const { payload, meta } = await dispatch(
      actions.settings.updatePersonThunk(data)
    );
    if (meta.requestStatus === "rejected") {
      return setError("email", payload as ErrorOption);
    }
    reset();
  };
  return (
    <Root>
      <Header>Personal Info</Header>
      <ChangePersonForm onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name="username"
          render={({ field: { onChange, onBlur, value }, fieldState }) => (
            <Input
              type="text"
              value={value}
              placeholder="Username"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <Controller
          control={control}
          name="email"
          render={({ field: { onChange, onBlur, value }, fieldState }) => (
            <Input
              type="email"
              value={value}
              placeholder="Email"
              fieldState={fieldState}
              onChange={onChange}
              onBlur={onBlur}
            />
          )}
        />
        <ChangeButton>
          <Button
            disabled={!isValid || isLoading}
            variant="primary"
            value="Save"
            isLoading={isLoading}
          />
        </ChangeButton>
      </ChangePersonForm>
    </Root>
  );
};

export default ChangePerson;

interface ChangePersonProps {}
const Root = styled.div`
  height: 800px;
  width: 90%;
`;
const Header = styled.div`
  ${TYPOGRAPHY.headerM}
  padding-top: 60px;
  padding-bottom: 30px;
  color: ${({ theme }) => theme.colors.color1};
`;
const ChangePersonForm = styled.form`
  width: 100%;
  @media ${devices.tablet} {
    width: 150%;
  }
  @media ${devices.mobileL} {
    width: 170%;
  }
`;
const ChangeButton = styled.div`
  width: 160px;
`;

import styled from "styled-components";

import { devices } from "assets";
import { Button, Status } from "components";
import { TYPOGRAPHY } from "styles";
import { Subscribe } from "types";

const Card: React.FC<CardProps> = ({
  isDisabled,
  subscribe,
  setActiveCard,
}) => {
  const { status, product, currentPeriodEnd } = subscribe;
  const toNormalDate = () => {
    const normDate = new Date(currentPeriodEnd * 1000);
    return [
      normDate.getDate(),
      normDate.getMonth(),
      normDate.getFullYear(),
    ].join(".");
  };
  return (
    <Root>
      <Header>
        <Logo>Gscore</Logo>
        <State>
          <Status status={status} />
        </State>
      </Header>
      <Border />
      <BottomPart>
        <License>
          <LicenseHead>{product.name} license</LicenseHead>
          <LicenseText>valid until {toNormalDate()}</LicenseText>
          <ViewButton>
            <Button onClick={setActiveCard} variant="secondary" value="View" />
          </ViewButton>
        </License>
        <Cost>
          <Price>${product.prices[0].price}</Price>
        </Cost>
      </BottomPart>
      <Disabled isDisabled={isDisabled} />
    </Root>
  );
};

export default Card;

interface CardProps {
  isDisabled?: boolean;
  subscribe: Subscribe;
  setActiveCard: () => void;
}

const Root = styled.div`
  background-color: ${({ theme }) => theme.colors.color7};
  width: 820px;
  height: 434px;
  border-radius: 12px;
  position: relative;
  @media ${devices.desktop} {
    width: 620px;
    height: 334px;
  }
  @media ${devices.laptopL} {
    width: 520px;
    height: 300px;
  }
  @media ${devices.laptop} {
    width: 450px;
    height: 280px;
  }
  @media ${devices.tablet} {
    width: 370px;
    height: 260px;
  }
  @media ${devices.mobileL} {
    width: 320px;
    height: 230px;
  }
  @media ${devices.mobileM} {
    width: 290px;
    height: 200px;
  }
  @media ${devices.mobileS} {
    width: 260px;
    height: 180px;
  }
`;
const Price = styled.div``;
const Header = styled.div`
  height: 108px;
  padding-top: 20px;
  ${TYPOGRAPHY.headerM};
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media ${devices.laptopL} {
    height: 100px;
  }
  @media ${devices.laptop} {
    height: 80px;
  }
  @media ${devices.tablet} {
    height: 90px;
    padding-top: 10px;
  }
  @media ${devices.mobileL} {
    padding-top: 0px;
    height: 70px;
  }
  @media ${devices.mobileM} {
    height: 60px;
  }
  @media ${devices.mobileS} {
    height: 40px;
  }
`;
const ViewButton = styled.div`
  z-index: 10;
  width: 120px;
`;
const Disabled = styled.div<{ isDisabled?: boolean }>`
  display: ${({ isDisabled }) => (isDisabled ? "block" : "none")};
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  position: absolute;
  background-color: rgb(39, 39, 39, 0.6);
`;
const Logo = styled.div`
  ${TYPOGRAPHY.headerM};
  padding-left: 40px;
  color: ${({ theme }) => theme.colors.color1};
  @media ${devices.tablet} {
    padding-left: 25px;
  }
  @media ${devices.mobileL} {
    font-size: 18px;
  }
`;
const State = styled.div`
  padding-right: 80px;
  color: ${({ theme }) => theme.colors.green};
  @media ${devices.tablet} {
    padding-right: 30px;
  }
`;
const Border = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.color5};
`;
const BottomPart = styled.div`
  display: flex;
  justify-content: space-evenly;
  height: 226px;
`;
const License = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: 250px;
  @media ${devices.laptopL} {
    padding-right: 120px;
  }
  @media ${devices.laptop} {
    padding-right: 50px;
  }
  @media ${devices.tablet} {
    padding-right: 30px;
  }
  @media ${devices.mobileM} {
    padding-right: 10px;
  }
`;
const LicenseHead = styled.div`
  ${TYPOGRAPHY.headerS}
  padding-top: 32px;
  color: ${({ theme }) => theme.colors.color1};
  @media ${devices.tablet} {
    font-size: 18px;
    padding-top: 20px;
  }
  @media ${devices.mobileM} {
    padding-top: 10px;
  }
`;
const LicenseText = styled.div`
  ${TYPOGRAPHY.link}
  padding-top: 12px;
  padding-bottom: 32px;
  color: ${({ theme }) => theme.colors.color5};
  @media ${devices.tablet} {
    padding-bottom: 15px;
  }
  @media ${devices.mobileM} {
    padding-top: 10px;
    padding-bottom: 15px;
  }
`;
const Cost = styled.div`
  ${TYPOGRAPHY.headerS}
  display: flex;
  padding-top: 32px;
  font-size: 24px;
  padding-right: 50px;
  color: ${({ theme }) => theme.colors.color1};
  @media ${devices.tablet} {
    padding-right: 30px;
    padding-left: 50px;
    font-size: 18px;
    padding-top: 20px;
  }
  @media ${devices.mobileM} {
    padding-right: 30px;
    padding-top: 10px;
  }
  @media ${devices.mobileS} {
    padding-left: 10px;
  }
`;

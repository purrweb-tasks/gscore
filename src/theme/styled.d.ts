import "styled-components";
import { COLORS } from "../styles";
import { FONTS } from "../styles/fonts";

declare module "styled-components" {
  export interface DefaultTheme {
    colors: typeof COLORS;
  }
}

import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import { ProductsResponseDto } from "./types";

export const getProductsThunk = createAsyncThunk<
  ProductsResponseDto,
  undefined,
  ExtraParamsThunkType<DefaultRejectValue>
>("products", async (_, { rejectWithValue }) => {
  try {
    const { data } = await api.get("products");

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

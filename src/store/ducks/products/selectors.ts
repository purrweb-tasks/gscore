import { createSelector } from "@reduxjs/toolkit";

import { RootState } from "../..";

export const getIsLoading = (state: RootState) => state.products.isLoading;

export const getProducts = (state: RootState) => state.products.products;

export const getProductById = (idProduct: number | null) =>
  createSelector(getProducts, (products) =>
    products.find((product) => product.id === idProduct)
  );

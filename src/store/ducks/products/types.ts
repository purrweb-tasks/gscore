import { DefaultRejectValue, Price, Product } from "types";

export type ProductsResponseDto = {
  id: number;
  sitesCount: number;
  name: string;
  prices: Price[];
}[];
export type InitialState = {
  isLoading: boolean;
  products: Product[];
  error: DefaultRejectValue;
};

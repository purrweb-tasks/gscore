import { AnyAction, createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import { getProductsThunk } from "./actions";
import { InitialState } from "./types";

const productSlice = createSlice({
  name: "products",
  initialState: {} as InitialState,
  reducers: {},
  extraReducers: (buider) => {
    buider
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return { ...state, ...action.payload.products };
      })
      .addCase(getProductsThunk.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getProductsThunk.fulfilled, (state, { payload }) => {
        state.products = payload;
        state.isLoading = false;
      })
      .addCase(getProductsThunk.rejected, (state) => {
        state.isLoading = false;
      });
  },
});

export const actions = { ...productSlice.actions, getProductsThunk };

export const reducer = productSlice.reducer;

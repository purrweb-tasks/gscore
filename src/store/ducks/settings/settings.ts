import { AnyAction, createSlice, isAnyOf } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import { updatePasswordThunk, updatePersonThunk } from "./actions";
import { InitialState } from "./types";

const settingsSlice = createSlice({
  name: "settings",
  initialState: {} as InitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return {
          ...state,
          ...action.payload.settings,
        };
      })
      .addMatcher(
        isAnyOf(updatePersonThunk.pending, updatePasswordThunk.pending),
        (state) => {
          state.isLoading = true;
        }
      )
      .addMatcher(
        isAnyOf(updatePersonThunk.fulfilled, updatePasswordThunk.fulfilled),
        (state, { payload }) => {
          state.user = payload;
          state.isLoading = false;
        }
      )
      .addMatcher(
        isAnyOf(updatePersonThunk.rejected, updatePasswordThunk.rejected),
        (state) => {
          state.isLoading = false;
        }
      );
  },
});

export const actions = {
  ...settingsSlice.actions,
  updatePersonThunk,
  updatePasswordThunk,
};

export const reducer = settingsSlice.reducer;

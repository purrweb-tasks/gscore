import { DefaultRejectValue, User } from "types";

export type InitialState = {
  user: User;
  isLoading: boolean;
  error?: DefaultRejectValue;
};
export type UpdatePersonDto = {
  email: string;
  username: string;
};
export type UpdatePasswordDto = {
  currentPassword: string;
  newPassword: string;
};
export type UpdateDataResponseDto = {} & User;

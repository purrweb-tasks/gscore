import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import {
  UpdatePersonDto,
  UpdateDataResponseDto,
  UpdatePasswordDto,
} from "./types";

export const updatePersonThunk = createAsyncThunk<
  UpdateDataResponseDto,
  UpdatePersonDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("users", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.patch("users", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});
export const updatePasswordThunk = createAsyncThunk<
  UpdateDataResponseDto,
  UpdatePasswordDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("users/updatePassword", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.patch("users/update-password", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

import { RootState } from "../..";

export const getIsLoading = (state: RootState) => state.settings.isLoading;

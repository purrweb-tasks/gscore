import { Code, DefaultRejectValue } from "types";

export type InitialState = {
  isLoading: boolean;
  code: Code;
  error?: DefaultRejectValue;
};
export type activateCodeDto = {
  code: string;
};
export type activeCodeResponseDto = {} & Code;

export type manageCodesDto = {
  codesIds: number[];
  subscribeId: number;
};

export type manageCodesResponseDto = {} & Code[];

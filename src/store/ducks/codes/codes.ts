import { AnyAction, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import { Code } from "types";

import { activeCodeThunk, manageCodesThunk } from "./actions";
import { InitialState } from "./types";

const codesSlice = createSlice({
  name: "codes",
  initialState: {} as InitialState,
  reducers: {
    setActiveCode: (
      state,
      {
        payload,
      }: PayloadAction<{
        activeCode: Code;
      }>
    ) => {
      state.code = payload.activeCode;
    },
  },
  extraReducers: (buider) => {
    buider
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return { ...state, ...action.payload.codes };
      })
      .addCase(activeCodeThunk.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(activeCodeThunk.fulfilled, (state, { payload }) => {
        state.code = payload;
        state.isLoading = false;
      })
      .addCase(activeCodeThunk.rejected, (state) => {
        state.isLoading = false;
      });
  },
});

export const actions = {
  ...codesSlice.actions,
  activeCodeThunk,
  manageCodesThunk,
};

export const reducer = codesSlice.reducer;

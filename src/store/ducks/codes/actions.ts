import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import {
  activateCodeDto,
  activeCodeResponseDto,
  manageCodesDto,
  manageCodesResponseDto,
} from "./types";

export const activeCodeThunk = createAsyncThunk<
  activeCodeResponseDto,
  activateCodeDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("code/activate", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.post("code/activate", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});
export const manageCodesThunk = createAsyncThunk<
  manageCodesResponseDto,
  manageCodesDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("code/manage", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.put("code/manage", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

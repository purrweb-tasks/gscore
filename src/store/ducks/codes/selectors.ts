import { RootState } from "../..";

export const getIsLoading = (state: RootState) => state.codes.isLoading;
export const getActiveCode = (state: RootState) => state.codes.code;

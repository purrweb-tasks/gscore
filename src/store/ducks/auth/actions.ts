import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import {
  SignUpDto,
  SignInDto,
  SignUpResponseDto,
  AuthResponseDto,
} from "./types";

export const signUpThunk = createAsyncThunk<
  SignUpResponseDto,
  SignUpDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("users/signUp", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.post("users/sign-up", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});
export const signInThunk = createAsyncThunk<
  AuthResponseDto,
  SignInDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("users/signIn", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.post("users/sign-in", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

import { RootState } from "../..";

export const getIsLoading = (state: RootState) => state.auth.isLoading;

export const getToken = (state: RootState) => state.auth.token;

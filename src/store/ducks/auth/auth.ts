import {
  AnyAction,
  createSlice,
  isAnyOf,
  PayloadAction,
} from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { destroyCookie, setCookie } from "nookies";

import { signInThunk, signUpThunk } from "./actions";
import { InitialState } from "./types";

const authSlice = createSlice({
  name: "auth",
  initialState: {} as InitialState,
  reducers: {
    addToken: (
      state,
      {
        payload,
      }: PayloadAction<{
        token: string;
      }>
    ) => {
      state.token = payload.token;
    },
    logout: (state) => {
      state.token = null;
      destroyCookie(null, "authToken");
    },
  },
  extraReducers: (buider) => {
    buider
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return { ...state, ...action.payload.auth };
      })
      .addMatcher(
        isAnyOf(signUpThunk.pending, signInThunk.pending),
        (state) => {
          state.isLoading = true;
        }
      )
      .addMatcher(
        isAnyOf(signUpThunk.fulfilled, signInThunk.fulfilled),
        (state, { payload }) => {
          state.token = payload.token;
          setCookie(null, "authToken", payload.token, {
            maxAge: 30 * 24 * 60 * 60,
            path: "/",
          });
          state.isLoading = false;
        }
      )
      .addMatcher(
        isAnyOf(signUpThunk.rejected, signInThunk.rejected),
        (state) => {
          state.isLoading = false;
        }
      );
  },
});

export const actions = { ...authSlice.actions, signUpThunk, signInThunk };

export const reducer = authSlice.reducer;

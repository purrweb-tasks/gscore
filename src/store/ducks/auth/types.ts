import { DefaultRejectValue, User } from "types";

export type SignInDto = {
  email: string;
  password: string;
};
export type SignUpDto = {
  username: string;
} & SignInDto;
export type SignUpResponseDto = {
  email: string;
  username: string;
  token: string;
};
export type AuthResponseDto = {
  token: string;
  user: User;
};
export type InitialState = {
  isLoading: boolean;
  token: string | null;
  error?: DefaultRejectValue;
};

import { DefaultRejectValue, User } from "types";

export type UserResponseDto = {
  id: string;
  email: string;
  username: string;
};
export type InitialState = {
  user: User | null;
  error?: DefaultRejectValue;
};

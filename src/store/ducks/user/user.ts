import { AnyAction, createSlice, isAnyOf } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import { signInThunk } from "../auth/actions";
import { updatePasswordThunk, updatePersonThunk } from "../settings/actions";
import { userGetMeThunk } from "./actions";
import { InitialState } from "./types";

const userSlice = createSlice({
  name: "user",
  initialState: {} as InitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return {
          ...state,
          ...action.payload.user,
        };
      })
      .addCase(signInThunk.fulfilled, (state, { payload }) => {
        state.user = payload.user;
      })
      .addCase(userGetMeThunk.fulfilled, (state, { payload }) => {
        state.user = payload;
      })
      .addCase(userGetMeThunk.rejected, (state) => {
        state.user = null;
      })
      .addMatcher(
        isAnyOf(updatePersonThunk.fulfilled, updatePasswordThunk.fulfilled),
        (state, { payload }) => {
          state.user = payload;
        }
      );
  },
});

export const actions = { ...userSlice.actions, userGetMeThunk };

export const reducer = userSlice.reducer;

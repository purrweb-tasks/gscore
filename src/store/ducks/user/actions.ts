import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import { UserResponseDto } from "./types";

export const userGetMeThunk = createAsyncThunk<
  UserResponseDto,
  undefined,
  ExtraParamsThunkType<DefaultRejectValue>
>("users/me", async (_, { rejectWithValue }) => {
  try {
    const { data } = await api.get("users/me");

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

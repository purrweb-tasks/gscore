import { DefaultRejectValue, Subscribe } from "types";

export type InitialState = {
  subsribe: Subscribe;
  isLoading: boolean;
  currentProductId: number;
  updatedSubcribeId: number | null;
  error?: DefaultRejectValue;
};

export type BuySubscribeDto = {
  priceId: number;
};
export type BuySubscribeResponseDto = {
  subsribe: Subscribe;
};

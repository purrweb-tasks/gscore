import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import { BuySubscribeDto, BuySubscribeResponseDto } from "./types";

export const buySubscribeThunk = createAsyncThunk<
  BuySubscribeResponseDto,
  BuySubscribeDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("payments/buy", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.post("payments/buy", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

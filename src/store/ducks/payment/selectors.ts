import { RootState } from "../..";

export const getUpdatedSubcribe = (state: RootState) =>
  state.payment.updatedSubcribeId;
export const getCurrentProduct = (state: RootState) =>
  state.payment.currentProductId;
export const getIsLoading = (state: RootState) => state.payment.isLoading;

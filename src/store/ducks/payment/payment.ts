import {
  AnyAction,
  createSlice,
  isAnyOf,
  PayloadAction,
} from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import { buySubscribeThunk } from "./actions";
import { InitialState } from "./types";

const paymentSlice = createSlice({
  name: "payment",
  initialState: {} as InitialState,
  reducers: {
    setUpdatedSubscribe: (
      state,
      {
        payload,
      }: PayloadAction<{
        updatedSubcribeId: number | null;
      }>
    ) => {
      state.updatedSubcribeId = payload.updatedSubcribeId;
    },
    setCurrentProductId: (
      state,
      {
        payload,
      }: PayloadAction<{
        currentProductId: number;
      }>
    ) => {
      state.currentProductId = payload.currentProductId;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return {
          ...state,
          ...action.payload.payment,
        };
      })
      .addCase(buySubscribeThunk.fulfilled, (state, { payload }) => {
        state.subsribe = payload.subsribe;
      })
      .addCase(buySubscribeThunk.pending, (state) => {
        state.isLoading = true;
      })
      .addMatcher(
        isAnyOf(buySubscribeThunk.fulfilled, buySubscribeThunk.rejected),
        (state) => {
          state.isLoading = false;
        }
      );
  },
});

export const actions = { ...paymentSlice.actions, buySubscribeThunk };

export const reducer = paymentSlice.reducer;

import { DefaultRejectValue, Subscribe } from "types";

export type InitialState = {
  isLoading: boolean;
  subscribes: Subscribe[];
  updatedSubscribe: ChangeSubscribeResponseDto;
  error: DefaultRejectValue;
};
export type SubscribeResponseDto = {} & Subscribe[];
export type ChangeSubscribeDto = {
  productId: number;
  subscribeId: number;
};
export type ChangeSubscribeResponseDto = {
  id: number;
  userId: number;
  productId: number;
  currentPeriodStart: string;
  currentPeriodEnd: string;
  status: string;
};

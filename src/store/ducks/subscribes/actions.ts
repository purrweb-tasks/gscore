import { createAsyncThunk } from "@reduxjs/toolkit";

import { api } from "api";
import { DefaultRejectValue, ExtraParamsThunkType } from "types";

import {
  ChangeSubscribeDto,
  ChangeSubscribeResponseDto,
  SubscribeResponseDto,
} from "./types";

export const getSubscribesThunk = createAsyncThunk<
  SubscribeResponseDto,
  undefined,
  ExtraParamsThunkType<DefaultRejectValue>
>("subscribe/self", async (_, { rejectWithValue }) => {
  try {
    const { data } = await api.get("subscribe/self");

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});
export const changeSubscribeThunk = createAsyncThunk<
  ChangeSubscribeResponseDto,
  ChangeSubscribeDto,
  ExtraParamsThunkType<DefaultRejectValue>
>("subscribe/changeProduct", async (requestData, { rejectWithValue }) => {
  try {
    const { data } = await api.post("subscribe/change-product", requestData);

    return data;
  } catch (error) {
    return rejectWithValue(error as DefaultRejectValue);
  }
});

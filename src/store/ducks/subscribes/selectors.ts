import { createSelector } from "@reduxjs/toolkit";

import { RootState } from "../..";

export const getIsLoading = (state: RootState) => state.subscribes.isLoading;

export const getSubscribes = (state: RootState) => state.subscribes.subscribes;
export const getSubscribeById = (idSubscribe: number | null) =>
  createSelector(getSubscribes, (subscribes) => {
    if (subscribes) {
      return subscribes.find((subscribe) => subscribe.id === idSubscribe);
    }
  });

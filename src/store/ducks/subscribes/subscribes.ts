import { AnyAction, createSlice, isAnyOf } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import { getSubscribesThunk, changeSubscribeThunk } from "./actions";
import { InitialState } from "./types";

const subscribeSlice = createSlice({
  name: "subscribes",
  initialState: {} as InitialState,
  reducers: {},
  extraReducers: (buider) => {
    buider
      .addCase(HYDRATE, (state, action: AnyAction) => {
        return { ...state, ...action.payload.subscribes };
      })
      .addCase(getSubscribesThunk.fulfilled, (state, { payload }) => {
        state.subscribes = payload;
        state.isLoading = false;
      })
      .addCase(changeSubscribeThunk.fulfilled, (state, { payload }) => {
        state.updatedSubscribe = payload;
        state.isLoading = false;
      })
      .addMatcher(
        isAnyOf(getSubscribesThunk.pending, changeSubscribeThunk.pending),
        (state) => {
          state.isLoading = true;
        }
      )
      .addMatcher(
        isAnyOf(getSubscribesThunk.rejected, changeSubscribeThunk.rejected),
        (state, { payload }) => {
          state.isLoading = false;
        }
      );
  },
});

export const actions = {
  ...subscribeSlice.actions,
  getSubscribesThunk,
  changeSubscribeThunk,
};

export const reducer = subscribeSlice.reducer;

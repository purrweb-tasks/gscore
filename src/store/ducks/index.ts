import { combineReducers } from "@reduxjs/toolkit";

import * as authDuck from "./auth";
import * as codesDuck from "./codes";
import * as paymentDuck from "./payment";
import * as productsDuck from "./products";
import * as settingsDuck from "./settings";
import * as subscribeDuck from "./subscribes";
import * as userDuck from "./user";

const rootReducer = combineReducers({
  user: userDuck.reducer,
  auth: authDuck.reducer,
  payment: paymentDuck.reducer,
  settings: settingsDuck.reducer,
  products: productsDuck.reducer,
  subscribes: subscribeDuck.reducer,
  codes: codesDuck.reducer,
});
export const actions = {
  user: userDuck.actions,
  auth: authDuck.actions,
  payment: paymentDuck.actions,
  settings: settingsDuck.actions,
  products: productsDuck.actions,
  subscribes: subscribeDuck.actions,
  codes: codesDuck.actions,
};
export const selectors = {
  user: userDuck.selectors,
  auth: authDuck.selectors,
  payment: paymentDuck.selectors,
  settings: settingsDuck.selectors,
  products: productsDuck.selectors,
  subscribes: subscribeDuck.selectors,
  codes: codesDuck.selectors,
};
export default rootReducer;

export const TYPOGRAPHY = {
  header: `
  font-family: 'THICCCBOI';
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 20px;
  `,
  headerL: `
  font-family: 'THICCCBOI';
  font-style: normal;
  font-weight: 700;
  font-size: 44px;  
  `,
  headerM: `
  font-family: 'THICCCBOI';
  font-style: normal;
  font-weight: 700;
  font-size: 28px;
  line-height: 40px;
  `,
  headerS: `
  font-family: 'THICCCBOI';
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 26px;
  `,
  footer: `
  font-family: 'Inter';
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  `,
  textBold: `
  font-family: 'THICCCBOI-BOLD';
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  `,
  paragraphBold: `
  font-family: 'THICCCBOI-MEDIUM';
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 20px;
  `,
  paragraph: `
  font-family: 'THICCCBOI';
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  `,
  link: `
  font-family: 'THICCCBOI';
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 18px;
  `,
};

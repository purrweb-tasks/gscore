import { useRouter } from "next/router";
import styled from "styled-components";

import { Facebook, LinkedIn, Twitter, devices, FullLogo } from "assets";
import { UserMenu } from "components";
import { selectors, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";

const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  const router = useRouter();
  const user = useAppSelector(selectors.user.getUser);
  return (
    <Root>
      <Header>
        <Logo onClick={() => router.push("/")}>
          <FullLogo />
        </Logo>
        <UserName>{user ? <UserMenu /> : null}</UserName>
      </Header>
      {children}
      <Footer>
        <BigBorder />
        <FooterDescription>
          <FullLogo />
          Ut enim ad minim veniam quis nostrud exercitation ea commodo
        </FooterDescription>
        <SmallBorder />
        <About>
          <AboutRights>
            Copyright © 2022 GScore | All Rights Reserved |
            <Links>
              <Link href="">Cookies</Link>|<Link href="">Privacy Policy</Link>
            </Links>
          </AboutRights>
          <Contacts>
            <Contact>
              <Facebook />
            </Contact>
            <Contact>
              <Twitter />
            </Contact>
            <Contact>
              <LinkedIn />
            </Contact>
          </Contacts>
        </About>
      </Footer>
    </Root>
  );
};

export default MainLayout;

interface MainLayoutProps extends React.HTMLAttributes<HTMLDivElement> {}

const Root = styled.div`
  width: 100vw;
  overflow: hidden;
  background-color: ${({ theme }) => theme.colors.color8};
`;
const Header = styled.header`
  padding-left: 100px;
  padding-top: 30px;
  display: flex;
  justify-content: space-between;
  padding-bottom: 60px;
  @media ${devices.laptop} {
    padding-left: 40px;
  }
  @media ${devices.mobileL} {
    padding-left: 20px;
  }
`;
const Logo = styled.button`
  padding: 0;
  background-color: transparent;
  border: none;
  cursor: pointer;
`;
const UserName = styled.div`
  color: ${({ theme }) => theme.colors.color1};
  ${TYPOGRAPHY.headerS};
`;
const Footer = styled.footer`
  padding-top: 50px;
  width: 100%;
`;
const FooterDescription = styled.div`
  margin-left: 86px;
  margin-bottom: 60px;
  padding-top: 50px;
  width: 280px;
  display: flex;
  flex-direction: column;
  ${TYPOGRAPHY.footer}
  justify-content: space-between;
  height: 170px;
  line-height: 30px;
  color: ${({ theme }) => theme.colors.color4};
  @media ${devices.laptop} {
    margin-left: 40px;
  }
  @media ${devices.tablet} {
    width: 300px;
  }
  @media ${devices.mobileL} {
    margin-left: 20px;
  }
  @media ${devices.mobileM} {
    margin-bottom: 30px;
  }
`;
const SmallBorder = styled.div`
  width: 88%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.color7};
  margin-left: 6vw;
  margin-right: 6vw;
  margin-bottom: 50px;
  @media ${devices.mobileM} {
    margin-bottom: 40px;
  }
`;
const BigBorder = styled.div`
  width: 100%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.color7};
`;
const About = styled.div`
  ${TYPOGRAPHY.footer}
  padding-left: 60px;
  padding-bottom: 32px;
  font-weight: 700;
  display: flex;
  justify-content: space-between;
  color: ${({ theme }) => theme.colors.color4};
  @media ${devices.tablet} {
    flex-direction: column;
    align-items: center;
    padding-left: 0px;
  }
`;
const AboutRights = styled.div`
  display: flex;
  @media ${devices.tablet} {
    margin-bottom: 20px;
    flex-direction: column;
    align-items: center;
  }
  @media ${devices.mobileM} {
    font-size: 14px;
  }
  @media ${devices.mobileS} {
    font-size: 12px;
  }
`;
const Links = styled.div`
  display: flex;
`;
const Link = styled.a`
  margin-left: 3px;
  margin-right: 3px;
  color: ${({ theme }) => theme.colors.color1};
  text-decoration: underline;
`;
const Contacts = styled.div`
  width: 220px;
  display: flex;
  justify-content: space-between;
  padding-right: 90px;
  @media ${devices.tablet} {
    padding-right: 0px;
  }
`;
const Contact = styled.button`
  padding: 0;
  border: none;
  font: inherit;
  color: inherit;
  background-color: transparent;
  cursor: pointer;
`;

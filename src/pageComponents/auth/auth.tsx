import { useState } from "react";

import { NextPage } from "next";
import { useRouter } from "next/router";
import styled from "styled-components";

import { devices } from "assets";
import { Checkout, Login, Register, Tab } from "components";
import { MainLayout } from "layouts";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";

enum NameTab {
  CREATE_ACCOUNT = "Create account",
  LOG_IN = "Log in",
  CHECKOUT = "Checkout",
}
const tabs = [NameTab.CREATE_ACCOUNT, NameTab.LOG_IN, NameTab.CHECKOUT];

const Auth: NextPage = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const startProduct = useAppSelector(selectors.products.getProducts)[0];
  const user = useAppSelector(selectors.user.getUser);
  const [registerStage, setRegisterStage] = useState(1);
  const handleBuyFirstSubscribe = async () => {
    await dispatch(
      actions.payment.buySubscribeThunk({ priceId: startProduct.id })
    );
    router.push("/subscriptions");
  };
  const handleRegisterStage = (newStage: number) => {
    setRegisterStage(newStage);
  };
  const renderCheckout = () => {
    if (user && registerStage != 3) {
      setRegisterStage(3);
    }
  };
  const renderAuth = () => {
    switch (registerStage) {
      case 1: {
        return <Register setNextStage={handleRegisterStage} />;
      }
      case 2: {
        return <Login setNextStage={handleRegisterStage} />;
      }
      case 3:
        return (
          <Checkout
            buySubscribe={handleBuyFirstSubscribe}
            product={startProduct}
          />
        );
    }
  };
  renderCheckout();
  return (
    <MainLayout>
      <AuthStage>
        <Stage>
          <Tab step={registerStage} tabs={tabs} />
          {renderAuth()}
        </Stage>
      </AuthStage>
    </MainLayout>
  );
};

export default Auth;

const AuthStage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  @media ${devices.tablet} {
    padding-left: 40px;
  }
`;
const Stage = styled.div``;

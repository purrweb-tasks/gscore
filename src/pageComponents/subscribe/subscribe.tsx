import { NextPage } from "next";
import { useRouter } from "next/router";
import styled from "styled-components";

import { Button, CardCheckout } from "components";
import { MainLayout } from "layouts";
import { actions, selectors, useAppDispatch, useAppSelector } from "store";
import { TYPOGRAPHY } from "styles";

const Subscribe: NextPage = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const productId = useAppSelector(selectors.payment.getCurrentProduct);
  const products = useAppSelector(selectors.products.getProducts);
  const currentProduct = useAppSelector(
    selectors.products.getProductById(productId)
  );
  const handleCurrentProduct = () => {
    if (!!!currentProduct) {
      return products[0];
    }
    return currentProduct;
  };
  const handleBuySubscribe = async () => {
    await dispatch(actions.payment.buySubscribeThunk({ priceId: productId }));
    router.push("/subscriptions");
  };
  return (
    <MainLayout>
      <SubscribeItem>
        <Subscription>
          <Header>Start your subscription</Header>
          <Message>
            We have sent you a payment receipt by e-mail and a link to download
            the plugin with a license key.
          </Message>
          <SubscribeCard>
            <CardCheckout product={handleCurrentProduct()} />
          </SubscribeCard>
          <GoSubscribe>
            <Button
              value="Go to my subscriptions"
              variant="primary"
              onClick={handleBuySubscribe}
            />
          </GoSubscribe>
        </Subscription>
      </SubscribeItem>
    </MainLayout>
  );
};

export default Subscribe;

const Header = styled.div`
  ${TYPOGRAPHY.headerL};
  padding-bottom: 30px;
  color: ${({ theme }) => theme.colors.color1};
`;
const SubscribeItem = styled.div`
  display: flex;
  justify-content: center;
`;
const Message = styled.div`
  ${TYPOGRAPHY.paragraph};
  color: ${({ theme }) => theme.colors.color1};
  padding-bottom: 30px;
`;
const SubscribeCard = styled.div``;
const GoSubscribe = styled.div`
  padding-top: 100px;
`;
const Subscription = styled.div``;

import { useEffect, useMemo, useState } from "react";

import { NextPage } from "next";
import { useRouter } from "next/router";
import Slider, { Settings } from "react-slick";
import styled from "styled-components";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { ArrowLeft, ArrowRight, Close, devices } from "assets";
import { Button, Card, CardLink } from "components";
import { MainLayout } from "layouts";
import {
  actions,
  selectors,
  useAppDispatch,
  useAppSelector,
  wrapper,
} from "store";
import { COLORS, TYPOGRAPHY } from "styles";
import { Code as CodeType } from "types";

const Subscriptions: NextPage = () => {
  const router = useRouter();
  const currentUpgradeSub = useAppSelector(
    selectors.payment.getUpdatedSubcribe
  );
  const dispatch = useAppDispatch();
  const subscriptions = useAppSelector(selectors.subscribes.getSubscribes);
  const isLoading = useAppSelector(selectors.codes.getIsLoading);
  const activeCode = useAppSelector(selectors.codes.getActiveCode);
  const [activeSub, setActiveSub] = useState(0);
  const [codesIds, setCodesIds] = useState<number[]>([]);
  let slider: Slider | null;

  const settings: Settings = {
    infinite: false,
    speed: 500,
    slidesToScroll: 1,
    variableWidth: true,
  };
  const handleUpdateSub = () => {
    dispatch(
      actions.payment.setUpdatedSubscribe({
        updatedSubcribeId: subscriptions[activeSub].id,
      })
    );
    router.push("/");
  };
  const handleSetActiveCard = async (indexCard: number) => {
    setActiveSub(indexCard);
  };
  const handleSelectDomain = (codeId: number, checked: boolean) => {
    if (!checked) {
      setCodesIds((codesIds) => [...codesIds, codeId]);
    } else {
      setCodesIds((codesIds) => codesIds.filter((id) => id !== codeId));
    }
  };
  const handleConfirm = async () => {
    await dispatch(
      actions.codes.manageCodesThunk({
        codesIds,
        subscribeId: subscriptions[activeSub].id,
      })
    );
    dispatch(actions.payment.setUpdatedSubscribe({ updatedSubcribeId: null }));
    await dispatch(actions.subscribes.getSubscribesThunk());
  };
  useEffect(() => {
    dispatch(actions.subscribes.getSubscribesThunk());
  }, [dispatch]);
  const codeIds: number[] = [];
  const pushIds = () => {
    subscriptions[activeSub]?.codes.map((code) => codeIds.push(code.id));
    codeIds.sort((a, b) => a - b);
  };
  const codes = useMemo(
    () => subscriptions[activeSub].codes,
    [activeSub, subscriptions]
  );
  pushIds();
  return (
    <MainLayout>
      <SubscriptionsItem>
        <Header>
          My subscriptions
          <UpgradeButton>
            <Button
              disabled={!!!subscriptions?.length}
              onClick={handleUpdateSub}
              value="Upgrade"
              variant="primary"
            />
          </UpgradeButton>
          <MobileUpgradeButton>
            <Button
              disabled={!!!subscriptions?.length}
              onClick={handleUpdateSub}
              value="Upgrade"
              variant="text"
            />
          </MobileUpgradeButton>
        </Header>
        {subscriptions?.length ? (
          <SubscribesSlider>
            <Slider {...settings} ref={(s) => (slider = s)}>
              {subscriptions.map((subscription, index) => (
                <CardItem key={index}>
                  <Card
                    setActiveCard={() => handleSetActiveCard(index)}
                    subscribe={subscription}
                    isDisabled={activeSub !== index}
                  />
                </CardItem>
              ))}
            </Slider>
            <ButtonArrows>
              <GoButton
                disabled={activeSub === 0}
                onClick={() => slider?.slickPrev()}
              >
                <ArrowLeft
                  strokeColor={activeSub === 0 ? COLORS.color7 : COLORS.color1}
                />
              </GoButton>
              <NumberOfSub>
                <CurrentNumberSub>{activeSub + 1}</CurrentNumberSub>/
                {subscriptions?.length}
              </NumberOfSub>
              <BackButton
                disabled={activeSub === subscriptions.length - 1}
                onClick={() => slider?.slickNext()}
              >
                <ArrowRight
                  strokeColor={
                    activeSub === subscriptions.length - 1
                      ? COLORS.color7
                      : COLORS.color1
                  }
                />
              </BackButton>
            </ButtonArrows>
            {currentUpgradeSub ? (
              <SelectDomainMobile>
                Select the domains you want to keep
              </SelectDomainMobile>
            ) : null}
            <Codes>
              {codeIds.map((id, index) => {
                const currentCode = codes.find((code) => code.id === id);
                const isChecked = codesIds.some((codeId) => codeId === id);
                const spareCode = subscriptions[activeSub].codes[0];
                return (
                  <Code key={index}>
                    <CardLink
                      isActivating={isLoading && id === activeCode.id}
                      selectDomain={(checked: boolean) =>
                        handleSelectDomain(id, checked)
                      }
                      checked={isChecked}
                      cardCode={currentCode || spareCode}
                    />
                    ;
                  </Code>
                );
              })}
            </Codes>
          </SubscribesSlider>
        ) : (
          <EmptyBlock>
            <EmptyIcon>
              <Close />
            </EmptyIcon>
            <EmptyHeader>No active subscriptions</EmptyHeader>
            <EmptyParagraph>
              You can subscribe right now by clicking on the button below
            </EmptyParagraph>
            <GetSubsButton>
              <Button
                onClick={() => router.push("/")}
                variant="primary"
                value="Get Gscore"
              />
            </GetSubsButton>
          </EmptyBlock>
        )}
        {currentUpgradeSub ? (
          <SelectDomain>
            <TextSelect>Select the domains you want to keep</TextSelect>
            <ConfirmButton>
              <Button
                disabled={
                  !!(
                    codesIds.length !==
                    subscriptions[activeSub]?.product.sitesCount
                  )
                }
                onClick={handleConfirm}
                value="Confirm"
                variant="primary"
              />
            </ConfirmButton>
          </SelectDomain>
        ) : null}
      </SubscriptionsItem>
    </MainLayout>
  );
};

export default Subscriptions;

export const getStaticSubProps = wrapper.getStaticProps((store) => async () => {
  store.dispatch(actions.subscribes.getSubscribesThunk());
  return { props: {} };
});
const SubscribesSlider = styled.div`
  overflow: hidden;
`;
const CurrentNumberSub = styled.div`
  color: ${({ theme }) => theme.colors.color1};
`;
const NumberOfSub = styled.div`
  display: flex;
  ${TYPOGRAPHY.headerM};
  padding-left: 10px;
  padding-right: 10px;
  color: ${({ theme }) => theme.colors.color7};
`;
const ButtonArrows = styled.div`
  display: flex;
  padding-top: 20px;
  @media ${devices.tablet} {
    justify-content: center;
  }
`;
const GoButton = styled.button`
  background-color: transparent;
  padding: 8px;
  border-radius: 12px;
  border: 1px solid ${({ theme }) => theme.colors.color5};
  cursor: pointer;
  :disabled {
    border-color: ${({ theme }) => theme.colors.color7};
  }
  @media ${devices.tablet} {
    display: none;
  }
`;
const BackButton = styled.button`
  background-color: transparent;
  padding: 8px;
  border-radius: 12px;
  border: 1px solid ${({ theme }) => theme.colors.color5};
  cursor: pointer;
  :disabled {
    border-color: ${({ theme }) => theme.colors.color7};
  }
  @media ${devices.tablet} {
    display: none;
  }
`;
const GetSubsButton = styled.div`
  width: 164px;
`;
const ConfirmButton = styled.div`
  width: 148px;
  @media ${devices.tablet} {
    width: 100%;
  }
`;
const UpgradeButton = styled.div`
  width: 152px;
  margin-right: 80px;
  @media ${devices.tablet} {
    display: none;
  }
`;
const MobileUpgradeButton = styled.div`
  display: none;
  margin-right: 10px;
  @media ${devices.tablet} {
    display: block;
  }
`;
const SubscriptionsItem = styled.div`
  padding-left: 100px;
  @media ${devices.laptop} {
    padding-left: 40px;
  }
  @media ${devices.mobileL} {
    padding-left: 20px;
  }
`;
const CardItem = styled.div`
  margin-right: 20px;
  @media ${devices.mobileL} {
    margin-right: 10px;
  }
`;
const Header = styled.div`
  ${TYPOGRAPHY.headerL};
  padding-bottom: 50px;
  display: flex;
  justify-content: space-between;
  color: ${({ theme }) => theme.colors.color1};
  @media ${devices.tablet} {
    ${TYPOGRAPHY.headerM};
  }
  @media ${devices.mobileL} {
    padding-right: 20px;
  }
  @media ${devices.mobileS} {
    ${TYPOGRAPHY.headerS};
  }
`;
const Codes = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 100px;
  @media ${devices.tablet} {
    align-items: center;
    padding-right: 40px;
  }
`;
const Code = styled.div`
  margin-bottom: 50px;
  width: 86vw;
  min-width: max-content;
  @media ${devices.tablet} {
    width: max-content;
  }
`;
const SelectDomain = styled.div`
  display: flex;
  justify-content: space-between;
  padding-right: 100px;
  align-items: center;
  @media ${devices.tablet} {
    padding-right: 40px;
  }
`;
const SelectDomainMobile = styled.div`
  ${TYPOGRAPHY.paragraph};
  color: ${({ theme }) => theme.colors.color1};
  display: none;
  @media ${devices.tablet} {
    display: block;
    padding-top: 50px;
  }
  @media ${devices.mobileL} {
    width: 180px;
  }
`;
const TextSelect = styled.div`
  ${TYPOGRAPHY.paragraph};
  color: ${({ theme }) => theme.colors.color1};
  @media ${devices.tablet} {
    display: none;
  }
`;
const EmptyBlock = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 500px;
`;
const EmptyIcon = styled.div`
  padding: 30px;
  padding-bottom: 20px;
  border-radius: 40px;
  background-color: ${({ theme }) => theme.colors.color7};
  margin-bottom: 20px;
`;
const EmptyHeader = styled.div`
  ${TYPOGRAPHY.headerM};
  color: ${({ theme }) => theme.colors.color1};
  padding-bottom: 10px;
  @media ${devices.mobileL} {
    ${TYPOGRAPHY.headerS};
  }
`;
const EmptyParagraph = styled.div`
  color: ${({ theme }) => theme.colors.color1};
  ${TYPOGRAPHY.link};
  padding-bottom: 40px;
  width: 260px;
  text-align: center;
`;

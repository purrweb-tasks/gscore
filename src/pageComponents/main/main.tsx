import { NextPage } from "next";
import styled from "styled-components";

import { devices } from "assets";
import { BuyLicenseCard, Button } from "components";
import { MainLayout } from "layouts";
import { actions, selectors, useAppSelector, wrapper } from "store";
import { TYPOGRAPHY } from "styles";

const Main: NextPage = () => {
  const products = useAppSelector(selectors.products.getProducts);
  return (
    <MainLayout>
      <Header>Get started with Gscore today!</Header>
      <Cards>
        {products.map((product) => {
          return <BuyLicenseCard key={product.id} product={product} />;
        })}
      </Cards>
      <ContactBlock>
        <Contact>Have more than 10 sites?</Contact>
        <Button isUnderline variant="text" value="Contact us" />
      </ContactBlock>
    </MainLayout>
  );
};

export default Main;

export const getStaticMainProps = wrapper.getStaticProps(
  (store) => async () => {
    store.dispatch(actions.products.getProductsThunk());
    return { props: {} };
  }
);

const Header = styled.div`
  ${TYPOGRAPHY.headerL};
  display: flex;
  justify-content: center;
  color: ${({ theme }) => theme.colors.color1};
  text-align: center;
  @media ${devices.mobileS} {
    ${TYPOGRAPHY.headerM};
  }
`;
const Cards = styled.div`
  display: flex;
  justify-content: space-evenly;
  padding-top: 100px;
  @media ${devices.laptop} {
    flex-direction: column;
    align-items: center;
  }
`;
const ContactBlock = styled.div`
  padding-top: 60px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Contact = styled.div`
  ${TYPOGRAPHY.paragraph};
  color: ${({ theme }) => theme.colors.color1};
`;

export { default as Auth } from "./auth";
export { default as Main, getStaticMainProps } from "./main";
export { default as Settings } from "./settings";
export { default as Subscribe } from "./subscribe";
export { default as Subscriptions, getStaticSubProps } from "./subscriptions";

import { useState } from "react";

import { NextPage } from "next";
import styled from "styled-components";

import { devices } from "assets";
import { ChangePassword, ChangePerson, CrossBarTab } from "components";
import { MainLayout } from "layouts";
import { TYPOGRAPHY } from "styles";

enum NameTab {
  PERSONAL_INFO = "Personal info",
  CHANGE_PASSWORD = "Change password",
}
const tabs = [NameTab.PERSONAL_INFO, NameTab.CHANGE_PASSWORD];

const Settings: NextPage = () => {
  const [activeTab, setActiveTab] = useState("Personal info");
  const handleActiveTab = (activeTab: string) => {
    setActiveTab(activeTab);
  };

  return (
    <MainLayout>
      <SettingsItem>
        <Header>Settings</Header>
        <CrossBarTab
          setActiveTab={handleActiveTab}
          tabs={tabs}
          activeTab={activeTab}
        />
        <SettingsForm>
          {activeTab === "Personal info" ? (
            <ChangePerson />
          ) : (
            <ChangePassword />
          )}
        </SettingsForm>
      </SettingsItem>
    </MainLayout>
  );
};

export default Settings;

const Header = styled.div`
  ${TYPOGRAPHY.headerL};
  padding-bottom: 50px;
  color: ${({ theme }) => theme.colors.color1};
`;
const SettingsItem = styled.div`
  padding-left: 100px;
  @media ${devices.tablet} {
    padding-left: 40px;
  }
`;
const SettingsForm = styled.div`
  width: 40vw;
`;

export type User = {
  username: string;
  email: string;
  id: string;
};
export type ExtraParamsThunkType<T> = {
  rejectValue: T;
};

export type DefaultRejectValue = {
  message: string;
  error: string;
  statusCode: number;
};

export type Subscribe = {
  id: number;
  userId: number;
  productId: number;
  currentPeriodStart: number;
  currentPeriodEnd: number;
  status: Status;
  product: Product;
  codes: Code[];
};
export type Status = "ACTIVE" | "INACTIVE" | "HOLD";

export type Price = {
  id: number;
  isActive: boolean;
  productId: number;
  price: string;
};
export type Product = {
  id: number;
  sitesCount: number;
  name: string;
  prices: Price[];
};
export type Code = {
  id: number;
  code: string;
  origin: string | null;
  status: Status;
  subscribeId: number;
  userId: number;
};

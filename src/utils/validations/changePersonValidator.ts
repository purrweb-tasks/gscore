import * as yup from "yup";
export const changePersonValidator = yup.object().shape({
  username: yup
    .string()
    .min(4, "Too short username")
    .required("Required field"),
  email: yup
    .string()
    .email("Incorrect email format")
    .required("Required field"),
});

import * as yup from "yup";
export const loginValidator = yup.object().shape({
  email: yup
    .string()
    .email("Incorrect email format")
    .required("Required field"),
  password: yup
    .string()
    .min(6, "Too short password")
    .required("Required field"),
});

import * as yup from "yup";
export const registerValidator = yup.object().shape({
  username: yup
    .string()
    .min(4, "Too short username")
    .required("Required field"),
  email: yup
    .string()
    .email("Incorrect email format")
    .required("Required field"),
  password: yup
    .string()
    .min(6, "Too short password")
    .required("Required field"),
});

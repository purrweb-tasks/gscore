import * as yup from "yup";
export const changePasswordValidator = yup.object().shape({
  currentPassword: yup
    .string()
    .min(6, "Too short password")
    .required("Required field"),
  newPassword: yup
    .string()
    .min(6, "Too short password")
    .required("Required field"),
});

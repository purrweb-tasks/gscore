export { loginValidator } from "./loginValidator";
export { registerValidator } from "./registerValidator";
export { changePersonValidator } from "./changePersonValidator";
export { changePasswordValidator } from "./changePasswordValidator";

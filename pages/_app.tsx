import type { AppProps } from "next/app";
import App from "next/app";
import { parseCookies } from "nookies";
import { PersistGate } from "redux-persist/integration/react";
import { ThemeProvider } from "styled-components";
import "styles/fonts.css";

import { wrapper, actions, persistor } from "store";
import { GlobalStyle } from "styles";
import { theme } from "theme";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <PersistGate loading={null} persistor={persistor}>
          <Component {...pageProps} />
        </PersistGate>
      </ThemeProvider>
    </>
  );
}
MyApp.getInitialProps = wrapper.getInitialAppProps(
  (store) => async (appContext) => {
    store.dispatch(
      actions.auth.addToken({ token: parseCookies(appContext.ctx).authToken })
    );
    await store.dispatch(actions.user.userGetMeThunk());
    await store.dispatch(actions.products.getProductsThunk());
    const appProps = await App.getInitialProps(appContext);
    return { ...appProps };
  }
);

export default wrapper.withRedux(MyApp);
